﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationFade : MonoBehaviour
{
	Vector2 target = new Vector2(500,1000);
	float speed = 4.0f;

	float timeUntilDeath = 4f;
	float transparency = 1f;
	
	void Update()
	{
		float step = speed * Time.deltaTime;
		timeUntilDeath -= Time.deltaTime;
		
		// move sprite towards the target location
		gameObject.GetComponent<Text>().color = new Color(0.9f,0.9f,0.9f,transparency);
		//Debug.Log("Okei");
		if(timeUntilDeath < 1.75f){
			speed = 300f;
			transparency -= Time.deltaTime*5;
		}
		transform.position = Vector2.MoveTowards(transform.position, target, step);
		
		if (timeUntilDeath <= 0f){
			Destroy(gameObject);
		}
	}
}
