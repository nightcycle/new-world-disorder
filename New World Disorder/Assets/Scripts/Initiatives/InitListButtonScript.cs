﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Initiatives;

public class InitListButtonScript : MonoBehaviour
{
	public GameObject percentButtonComplete;
	public GameObject localTitle;
	string currentInitiativeTitle;
	// Start is called before the first frame update
	public GameObject titleText;
	public GameObject descriptionText;
	public GameObject percentComplete;
	public GameObject timeText;
	public GameObject template;
	public GameObject startButton;

	// remaining construction cost
	public GameObject constructMoneyTotalText;
	public GameObject constructMilitaryTotalText;
	public GameObject constructApprovalTotalText;
	
	public GameObject constructMoneyUpfrontText;
	public GameObject constructMilitaryUpfrontText;
	public GameObject constructApprovalUpfrontText;
	
	// remaining benefit rates
	public GameObject benefitMoneyRateText;
	public GameObject benefitMilitaryRateText;
	public GameObject benefitApprovalRateText;	
	
	public GameObject scrollViewContent;
	public GameObject initiativeButtonTemplate;
	
	//Initiative
	public Initiative init;
	
	GameObject prop;
	
    void Start()
    {
		Transform LTI = template.transform.parent.parent.parent.parent;
		GetComponent<Button>().onClick.AddListener(click);
		prop = getChild(LTI.gameObject, "PropertiesPanel");
		titleText = getChild(prop, "TitleText");
		descriptionText = getChild(prop, "DescText");		
		percentComplete = getChild(prop, "PercentText");	
		timeText = getChild(prop, "TimeText");	
		startButton = getChild(prop, "StartButton");
		
		GameObject construct = getChild(prop, "Construction");
		constructMoneyTotalText = getChild(construct, "MoneyText");	
		constructMilitaryTotalText = getChild(construct, "MilitaryText");	
		constructApprovalTotalText = getChild(construct, "ApprovalText");
		
		constructMoneyUpfrontText = getChild(construct, "MoneyUpfront");	
		constructMilitaryUpfrontText = getChild(construct, "MilitaryUpfront");	
		constructApprovalUpfrontText = getChild(construct, "ApprovalUpfront");
		
		percentButtonComplete = getChild(template, "PercentComplete");
		localTitle = getChild(template, "TitleText");
		currentInitiativeTitle = localTitle.GetComponent<Text>().text;
		
		GameObject benefit = getChild(prop, "Benefit");
		benefitMoneyRateText = getChild(benefit, "MoneyText");
		benefitMilitaryRateText = getChild(benefit, "MilitaryText");
		benefitApprovalRateText = getChild(benefit, "ApprovalText");
		
		scrollViewContent = getChild(getChild(getChild(LTI.gameObject, "Viewport"), "VClip"), "Content");
	}

	void click()
	{
		InitiativeUI.currentInitiative = localTitle.GetComponent<Text>().text;
		prop.SetActive(true);
		updateProperty();
	}

	GameObject getChild(GameObject Obj, string name)
	{
		//Debug.Log(Obj.name);
		foreach(Transform o in Obj.transform){
			//Debug.Log(o.gameObject.name + " : " + name);
			if (o.gameObject.name == name)
			{
				return o.gameObject;
			}
		}
		return null;
	}

	public string timeToText(float duration)
	{
		int minutes = (int)Mathf.Floor(duration/60f);
		int seconds = (int)Mathf.Floor(((float)duration-(Mathf.Floor(duration/60f)*60f)));

		if(seconds.ToString().Length == 1)
		{
			return minutes + ":0"+seconds;
		}
		else
		{
			return minutes + ":" + seconds;
		}
		
	}
	
	
	public void updateProperty()
	{
		float progress = init.constructionProgress/init.constructionDuration;
		
		titleText.GetComponent<Text>().text = init.title;
		string actualDesc = init.description;
		if (init.unlocks.Length > 0){
			actualDesc = actualDesc + " Unlocks: ";
			foreach(string nextInit in init.unlocks){
				actualDesc = actualDesc + nextInit + ", ";
			}
		}
		actualDesc = actualDesc.Substring(0, actualDesc.Length-2) + ".";
		descriptionText.GetComponent<Text>().text = actualDesc;
		
		percentComplete.GetComponent<Text>().text = Mathf.Round(100*init.constructionProgress/init.constructionDuration) + "%"; 
		timeText.GetComponent<Text>().text = timeToText(init.constructionDuration-init.constructionProgress);
		
		//construct
		float constructMoneyInitCost = init.initialConstructionCost.x;
		float constructMoneyRateTotalRemaining = (init.constructRates.x*init.constructionDuration/60f);
		
		float constructMilitaryInitCost = init.initialConstructionCost.y;
		float constructMilitaryRateTotalRemaining = (init.constructRates.y*init.constructionDuration/60f);
	
		float constructApprovalInitCost = init.initialConstructionCost.z;
		float constructApprovalRateTotalRemaining = (init.constructRates.z*init.constructionDuration/60f);
		
		if (init.constructionStarted == true){
			constructMoneyInitCost = 0;
			constructMilitaryInitCost = 0;
			constructApprovalInitCost = 0;
			constructMoneyUpfrontText.GetComponent<Text>().text = "";
			constructMilitaryUpfrontText.GetComponent<Text>().text = "";
			constructApprovalUpfrontText.GetComponent<Text>().text = "";
			startButton.SetActive(false);
		}else{
			startButton.SetActive(true);
			constructMoneyUpfrontText.GetComponent<Text>().text = "(" + Mathf.Round(init.initialConstructionCost.x) + " upfront)";
			constructMilitaryUpfrontText.GetComponent<Text>().text = "(" + Mathf.Round(init.initialConstructionCost.y) + " upfront)";
			constructApprovalUpfrontText.GetComponent<Text>().text = "(" + Mathf.Round(init.initialConstructionCost.z) + " upfront)";	
		}
		
		constructMoneyTotalText.GetComponent<Text>().text = "" + Mathf.Round(((1-progress)*constructMoneyRateTotalRemaining)+constructMoneyInitCost);
		constructMilitaryTotalText.GetComponent<Text>().text = "" + Mathf.Round(((1-progress)*constructMilitaryRateTotalRemaining)+constructMilitaryInitCost);
		constructApprovalTotalText.GetComponent<Text>().text = "" + Mathf.Round(((1-progress)*constructApprovalRateTotalRemaining*10f)/10f+constructApprovalInitCost)+"%";

		//benefits
		benefitMoneyRateText.GetComponent<Text>().text = init.earnRates.x + "/m";
		benefitMilitaryRateText.GetComponent<Text>().text = init.earnRates.y + "/m";
		benefitApprovalRateText.GetComponent<Text>().text = init.earnRates.z + "%/m";		
	}	
	
    public void spawnInitiatives(Initiative[] currentInitiatives)
    {
		foreach (Transform sVC in scrollViewContent.transform)
		{
			if(sVC.gameObject != gameObject){
				Destroy(sVC.gameObject);
			}
		}
		//Debug.Log("Spawn");
		foreach (Initiative currentInit in currentInitiatives)
        {
		   //Debug.Log(currentInit.title + " is " + currentInit.unlocked);
		   //Debug.Log("Yes");
		   bool recentlyUnlocked = true;
		   
		   if (currentInit.unlocked == true && currentInit.constructionComplete == false)
           {
				GameObject button = Instantiate(initiativeButtonTemplate, template.transform.parent.parent.parent);
				button.SetActive(true);
				button.transform.parent = scrollViewContent.transform;
				button.GetComponent<InitListButtonScript>().init = currentInit;
				getChild(button, "TitleText").GetComponent<Text>().text = currentInit.title;
				//Debug.Log("Title: " + currentInit.title);
				getChild(button, "DescText").GetComponent<Text>().text = currentInit.description;
				getChild(button, "PercentComplete").GetComponent<Text>().text = 100*currentInit.constructionProgress/currentInit.constructionDuration + "%"; 
				GameObject open = getChild(button, "Button");
				//Debug.Log("Added");
				//open.GetComponent<Button>().onClick.AddListener(delegate {updateProperty(currentInit); });
            }
			
        }
		Destroy(gameObject);
    }	
	float delay = 0.25f;
	void Update()
	{
		percentButtonComplete.GetComponent<Text>().text = Mathf.Round(100*init.constructionProgress/init.constructionDuration) + "%"; 
		if (InitiativeUI.currentInitiative == currentInitiativeTitle)
		{
			updateProperty();
			
			if(init != null && Mathf.Round(100*init.constructionProgress/init.constructionDuration) >= 100 && prop.activeSelf == true){
				delay -= Time.deltaTime;
				if (delay < 0){
					delay = 0.25f;
					spawnInitiatives(InitiativeManager.initiatives);
					prop.SetActive(false);
				}
			}
		}
	}
}
