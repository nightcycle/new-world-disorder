﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Initiatives;

public class InitiativeUI : MonoBehaviour
{
    public GameObject initiativeUI;
    public GameObject doneButton;
    public GameObject scrollViewContent;
    public GameObject settingsButton;
    public GameObject governorButton;
    public GameObject initiativeButton;
	public GameObject initiativeButtonTemplate;
    public GameObject traitVisualizer;

	public GameObject titleText;
	public GameObject descriptionText;
	public GameObject percentComplete;
	public GameObject timeText;
	
	public GameObject propPanel;
	
	public static string currentInitiative;
	
	RectTransform contentRectTransform;
  
    // Start is called before the first frame update
    void Start()
    {
		currentInitiative = "";
		contentRectTransform = scrollViewContent.GetComponent<RectTransform>(); 
		initiativeButton.GetComponent<Button>().onClick.AddListener(setUIActive);
		doneButton.GetComponent<Button>().onClick.AddListener(setUIInactive);
    }
	
	float timeSinceClick = 0f;
	
	void Update()
	{
		timeSinceClick = timeSinceClick + Time.deltaTime;
	}

    //This function makes the scroll view and done button visible
    public void setUIActive()
    {
		GlobeScript.earthLocked = true;
		if (timeSinceClick >  1f){
			timeSinceClick = 0f;
			spawnInitiatives(InitiativeManager.initiatives);

            settingsButton.SetActive(false);
            governorButton.SetActive(false);
            initiativeButton.SetActive(false);
            traitVisualizer.SetActive(false);

			initiativeUI.SetActive(true);
			doneButton.SetActive(true);
		}		
    }

    //This function makes the scroll view and done button invisible
    public void setUIInactive()
    {
		GlobeScript.earthLocked = false;
		settingsButton.SetActive(true);
        governorButton.SetActive(true);
        initiativeButton.SetActive(true);
        traitVisualizer.SetActive(true);

        propPanel.SetActive(false);
		initiativeUI.SetActive(false);
        doneButton.SetActive(false);
    }
	
	public string timeToText(float duration)
	{
		int minutes = (int)Mathf.Floor(duration/60f);
		int seconds = (int)Mathf.Floor(((float)duration-(Mathf.Floor(duration/60f)*60f)));

		if(seconds.ToString().Length == 1)
		{
			return minutes + ":"+seconds+"0";
		}
		else
		{
			return minutes + ":" + seconds;
		}
		
	}

	public void updateProperty(Initiative init)
	{
		//Debug.Log("Clicky");
		//titleText.GetComponent<Text>().text = init.title;
		//descriptionText.GetComponent<Text>().text = init.description; 
		//percentComplete.GetComponent<Text>().text = 100*init.constructionProgress/init.constructionDuration + "%"; 
		//timeText.GetComponent<Text>().text = timeToText(init.constructionDuration-init.constructionProgress);
		
	}

	GameObject getChild(GameObject Obj, string name)
	{
		//Debug.Log(Obj.name);
		foreach(Transform o in Obj.transform){
			//Debug.Log(o.gameObject.name + " : " + name);
			if (o.gameObject.name == name)
			{
				return o.gameObject;
			}
		}
		return null;
	}
	
    public void spawnInitiatives(Initiative[] currentInitiatives)
    {
		foreach (Transform sVC in scrollViewContent.transform)
		{
			Destroy(sVC.gameObject);
		}
		foreach (Initiative currentInit in currentInitiatives)
        {
		   //Debug.Log(currentInit.title + " is " + currentInit.unlocked);
		   if (currentInit.unlocked == true && currentInit.constructionComplete == false)
           {
				GameObject button = Instantiate(initiativeButtonTemplate, initiativeButton.transform.parent);
				button.SetActive(true);
				button.transform.parent = scrollViewContent.transform;
				button.GetComponent<InitListButtonScript>().init = currentInit;
				getChild(button, "TitleText").GetComponent<Text>().text = currentInit.title;
				getChild(button, "DescText").GetComponent<Text>().text = currentInit.description;
				getChild(button, "PercentComplete").GetComponent<Text>().text = 100*currentInit.constructionProgress/currentInit.constructionDuration + "%"; 
				GameObject open = getChild(button, "Button");
				
				//open.GetComponent<Button>().onClick.AddListener(delegate {updateProperty(currentInit); });
            }
        }
    }

}
