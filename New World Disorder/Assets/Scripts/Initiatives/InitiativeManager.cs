﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Initiatives;
using Governors;
 
public class InitiativeManager : MonoBehaviour
{
	
	public static Initiative i1;
	public static Initiative i2; 
	public static Initiative i3;
	public static Initiative i4;
	public static Initiative i5;
	public static Initiative i6;
	public static Initiative i7;
	public static Initiative i8;
	public static Initiative i9;
	public static Initiative i10;
	public static Initiative i11;
	public static Initiative i12;
	public static Initiative i13;
	public static Initiative i14;
	public static Initiative i15;
	public static Initiative i16;
	public static Initiative i17;
	public static Initiative i18;
	public static Initiative i19;
	public static Initiative i20;
	public static Initiative i21;
	public static Initiative i22;
	public static Initiative i23;
	public static Initiative i24;
	public static Initiative i25;
	public static Initiative i26;
	public static Initiative i27;
	public static Initiative i28;
	public static Initiative i29;
	public static Initiative i30;
	public static Initiative i31;
	public static Initiative i32;
	public static Initiative i33;
	public static Initiative i34;
	public static Initiative i35;
	public static Initiative i36;
	public static Initiative i37;
	public static Initiative i38;
	public static Initiative i39;
	public static Initiative i40;
	public static Initiative i41;
	public static Initiative i42;
	public static Initiative i43;
	public static Initiative i44;
	public static Initiative i45;
	public static Initiative i46;
	public static Initiative i47;
	public static Initiative i48;
	public static Initiative i49;
	public static Initiative i50;
	public static Initiative i51;
	public static Initiative i52;
	public static Initiative i53;
	public static Initiative i54;
	public static Initiative i55;
	public static Initiative i56;
	public static Initiative i57;
	public static Initiative i58;
	
	
	
	//put all initiatives in here
	public static Initiative[] initiatives;
	
	public Initiative GetInitiative(string title)
	{
		foreach(Initiative init in initiatives)
		{
			if (init.title == title)
			{
				return init;
			}
		}
		int i = Random.Range(0, initiatives.Length-1); //getting the array length for some reason broke it so just update this number to be the amount of inits - 1
		Initiative finalInit = initiatives[i];
		return finalInit;
	}
	
	public void startInitiative()
	
	{
		foreach(Initiative init in initiatives)
		{
			if (init.title == propTitle.GetComponent<Text>().text)
			{
				init.constructionStarted = true;
				Resources.currency -= init.initialConstructionCost.x;
				Resources.enforcement -= init.initialConstructionCost.y;
				Resources.legitimacy -= init.initialConstructionCost.z;
			}
		}
	}

	public GameObject startButton;
	public GameObject propTitle;
	
	//adds the tsv data to initiative
	Initiative fillInitiative(Initiative babyInit){
		string tsvFilePath = "Assets/TSVs/LTI.tsv";
		
		int row = TSVScript.findRow(tsvFilePath, babyInit.title, 1);

		babyInit.description = TSVScript.read(tsvFilePath, row, 4);
		babyInit.level = (int)float.Parse(TSVScript.read(tsvFilePath, row, 3));
		float levelMultiplier = Mathf.Pow(1.25f,(float.Parse(TSVScript.read(tsvFilePath, row, 3))-1));
		babyInit.constructionDuration = 0.1f*float.Parse(TSVScript.read(tsvFilePath, row, 11));
		babyInit.initialConstructionCost = new Vector3(0, 0, 0);
		float costMultiplier = 1000f;
		
		babyInit.constructRates = new Vector3(Mathf.Round(levelMultiplier*costMultiplier*float.Parse(TSVScript.read(tsvFilePath, row, 6))/(babyInit.constructionDuration/60)), 0, Mathf.Round(levelMultiplier*costMultiplier*0.01f*float.Parse(TSVScript.read(tsvFilePath, row, 7))/(babyInit.constructionDuration/60)*10)/10);		
		float earnMultiplier = 120;
		babyInit.earnRates = new Vector3(Mathf.Round(levelMultiplier*earnMultiplier*float.Parse(TSVScript.read(tsvFilePath, row, 8))), 0, Mathf.Round(levelMultiplier*earnMultiplier*0.1f*float.Parse(TSVScript.read(tsvFilePath, row, 9))*10)/10);				
		babyInit.initialRewards = new Vector3(0, 0, 0);	
		babyInit.constructionProgress = 0f;
		
		float populist = float.Parse(TSVScript.read(tsvFilePath, row, 12));
		float honest = float.Parse(TSVScript.read(tsvFilePath, row, 13));
		float divisive = float.Parse(TSVScript.read(tsvFilePath, row, 14));
		float violent = float.Parse(TSVScript.read(tsvFilePath, row, 15));
		float autocrat = float.Parse(TSVScript.read(tsvFilePath, row, 16));
		float altruist = float.Parse(TSVScript.read(tsvFilePath, row, 17));
		float corrupt = float.Parse(TSVScript.read(tsvFilePath, row, 18));
		
		babyInit.traits = new TraitPack(violent, autocrat, altruist, corrupt, populist, divisive, honest);
		babyInit.weight = float.Parse(TSVScript.read(tsvFilePath, row, 10));
	
		return babyInit;
	}
	
	void Start()
	{
		//level 1
		Initiative i1 = fillInitiative(new Initiative("Police Force", true, new string [] {"SWAT Team"}));
		Initiative i2 = fillInitiative(new Initiative("Local Militias", true, new string [] {"Organized Military"}));
		Initiative i3 = fillInitiative(new Initiative("Local Jails", true, new string [] {"Prisons"}));		
		Initiative i4 = fillInitiative(new Initiative("Farming", true, new string [] {"GMO Assisted Farming"}));
		Initiative i5 = fillInitiative(new Initiative("Mining", true, new string [] {"Automated Mining"}));		
		Initiative i6 = fillInitiative(new Initiative("Handcrafted Products", true, new string [] {"Mass Production"}));		
		Initiative i7 = fillInitiative(new Initiative("Human Government", true, new string [] {"Internet Facilitated Government"}));	

		//level 2
		Initiative i8 = fillInitiative(new Initiative("Private Health Care", false, new string [] {"Medicare"}));
		Initiative i9 = fillInitiative(new Initiative("Food Stamp", false, new string [] {"Homeless Shelters"}));	
		Initiative i10 = fillInitiative(new Initiative("Global Radio Network", false, new string [] {"Global Telephone Network"}));		
		Initiative i11 = fillInitiative(new Initiative("Energy Grid", false, new string [] {"Natural Gas Energy"}));		
		Initiative i12 = fillInitiative(new Initiative("Railroad Network", false, new string [] {"Highway Network"}));	
		Initiative i13 = fillInitiative(new Initiative("Public Schooling", false, new string [] {"College Educated Teachers", "College Scholarships for Underpriviledged", "Propagandize School Curriculum"}));	
		Initiative i14 = fillInitiative(new Initiative("Organized Military", false, new string [] {"Armored Military", "Work Camps"}));	
		Initiative i15 = fillInitiative(new Initiative("Prisons", false, new string [] {"Work Camps"}));	
		Initiative i16 = fillInitiative(new Initiative("Political Rallies", false, new string [] {"Propagandize School Curriculum", "Anti-Opposition Propaganda", "Normalize Government Rhetoric"}));	
				
		//level 3
		Initiative i17 = fillInitiative(new Initiative("Global Telephone Network", false, new string [] {"Global Internet"}));	
		Initiative i18 = fillInitiative(new Initiative("Natural Gas Energy", false, new string [] {"Fission Energy"}));		
		Initiative i19 = fillInitiative(new Initiative("College Educated Teacher", false, new string [] {"Public School Extra Curricular Activities"}));	
		Initiative i20 = fillInitiative(new Initiative("College Scholarships for Underpriviledged", false, new string [] {"Free College"}));	
		Initiative i21 = fillInitiative(new Initiative("SWAT Team", false, new string [] {"Secret Police"}));	
		Initiative i22 = fillInitiative(new Initiative("Anti-Opposition Propaganda", false, new string [] {"Persecution of Opposition"}));			
		Initiative i23 = fillInitiative(new Initiative("Normalize Regime Rhetoric", false, new string [] {"Persecution of Opposition"}));			
		Initiative i24 = fillInitiative(new Initiative("GMO Assisted Farming", false, new string [] {"Automated Farming"}));	
		Initiative i25 = fillInitiative(new Initiative("Mass Production", false, new string [] {"Automated Mass Production"}));	
		
		//level 4
		Initiative i26 = fillInitiative(new Initiative("Medicaid", false, new string [] {"Free Healthcare"}));	
		Initiative i27 = fillInitiative(new Initiative("Homeless Shelter", false, new string [] {"Universal Basic Income"}));	
		Initiative i28 = fillInitiative(new Initiative("Global Internet", false, new string [] {"Global 5G Data", "Internet Facilitated Government"}));	
		Initiative i29 = fillInitiative(new Initiative("Fission Energy", false, new string [] {"Net Zero Carbon Emissions"}));	
		Initiative i30 = fillInitiative(new Initiative("Highway Network", false, new string [] {"Drone Network"}));	
		Initiative i31 = fillInitiative(new Initiative("Public School Extra Curricular Acitivities", false, new string [] {"School Bus System"}));	
		Initiative i32 = fillInitiative(new Initiative("School Bus System", false, new string [] {}));	
		Initiative i33 = fillInitiative(new Initiative("Armored Military", false, new string [] {"Nuclear Weapons"}));	
		Initiative i34 = fillInitiative(new Initiative("Propagandize School Curriculum", false, new string [] {"Reeducation Camps"}));	
		Initiative i35 = fillInitiative(new Initiative("Persecution of Opposition", false, new string [] {"Eliminate Opposition"}));	
		Initiative i36 = fillInitiative(new Initiative("Eliminate Opposition", false, new string [] {}));	
		Initiative i37 = fillInitiative(new Initiative("Automated Farming", false, new string [] {"Vertical Farming"}));	
		Initiative i38 = fillInitiative(new Initiative("Automated Mining", false, new string [] {}));	
		Initiative i39 = fillInitiative(new Initiative("Internet Facillitated Government", false, new string [] {"AI Run Government"}));	
		
		//level 5
		Initiative i40 = fillInitiative(new Initiative("Global 5G Data", false, new string [] {"Free Internet"}));	
		Initiative i41 = fillInitiative(new Initiative("Net Zero Carbon Emissions", false, new string [] {"Free Electricity"}));	
		Initiative i42 = fillInitiative(new Initiative("Drone Network", false, new string [] {"Automated Transportation"}));	
		Initiative i43 = fillInitiative(new Initiative("Free College", false, new string [] {}));	
		Initiative i44 = fillInitiative(new Initiative("Secret Police", false, new string [] {}));	
		Initiative i45 = fillInitiative(new Initiative("Work Camps", false, new string [] {"Death Camps", "Reeducation Camps"}));	
		Initiative i46 = fillInitiative(new Initiative("Normalize Oppression", false, new string [] {"Normalize Mass Executions"}));	
		Initiative i47 = fillInitiative(new Initiative("Automated Mass Production", false, new string [] {}));	

		//level 6
		Initiative i48 = fillInitiative(new Initiative("Free Healthcare", false, new string [] {}));	
		Initiative i49 = fillInitiative(new Initiative("Universal Basic Income", false, new string [] {}));	
		Initiative i50 = fillInitiative(new Initiative("Free Internet", false, new string [] {}));	
		Initiative i51 = fillInitiative(new Initiative("Free Electricity", false, new string [] {}));	
		Initiative i52 = fillInitiative(new Initiative("Automated Transportation", false, new string [] {}));	
		Initiative i53 = fillInitiative(new Initiative("Nuclear Weapon", false, new string [] {}));	
		Initiative i54 = fillInitiative(new Initiative("Death Camps", false, new string [] {}));	
		Initiative i55 = fillInitiative(new Initiative("Reeducation Camps", false, new string [] {}));			
		Initiative i56 = fillInitiative(new Initiative("Normalize Mass Executions", false, new string [] {}));	
		Initiative i57 = fillInitiative(new Initiative("Vertical Farming", false, new string [] {}));	
		Initiative i58 = fillInitiative(new Initiative("AI Run Government", false, new string [] {}));	
		
		//put all initiatives in here
		initiatives = new Initiative[]{i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,i11,i12,i13,i14,i15,i16,i17,i18,i19,i20,i21,i22,i23,i24,i25,i26,i27,i28,i29,i30,i31,i32,i33,i34,i35,i36,i37,i38,i39,i40,i41,i42,i43,i44,i45,i46,i47,i48,i49,i50,i51,i52,i53,i54,i55,i56,i57,i58};
	
		startButton.GetComponent<Button>().onClick.AddListener(startInitiative);	
	}

	float timeSinceLastResourceAudit = 0f;
	float auditSeparation = 1f;
	float countDownToLevel2 = 10f;
	
	void Update()
	{
		countDownToLevel2= countDownToLevel2-Time.deltaTime;
		if (countDownToLevel2 <= 0f){
			foreach(Initiative i in initiatives){
				if (i.level == 2 && i.title != "Organized Military" && i.title != "Prisons"){
					i.unlocked = true;
				}
			}
		}
		
		float delta = Time.deltaTime;
		timeSinceLastResourceAudit += delta;
		if (timeSinceLastResourceAudit >= auditSeparation)
		{
			timeSinceLastResourceAudit = 0f;
			foreach(Initiative init in initiatives)
			{
				if (init.constructionComplete == false && init.constructionStarted == true)
				{
					Resources.currency -= init.constructRates.x*(auditSeparation/60);
					Resources.enforcement -= init.constructRates.y*(auditSeparation/60);
					Resources.legitimacy -= init.constructRates.z*(auditSeparation/60);
				}
				if (init.constructionComplete == true)
				{
					init.timeSinceConstructionCompleted = init.timeSinceConstructionCompleted + auditSeparation;
					
					float halfLife = 75f;
					float adjustment = Mathf.Pow(0.5f,init.timeSinceConstructionCompleted/halfLife);
					
					Resources.currency += adjustment*init.earnRates.x*(auditSeparation/60);
					Resources.enforcement += adjustment*init.earnRates.y*(auditSeparation/60);
					Resources.legitimacy += adjustment*init.earnRates.z*(auditSeparation/60);
				}
			}
		}
		if (initiatives != null){
			foreach(Initiative init in initiatives)
			{
				if (init.constructionComplete == false && init.constructionStarted == true)
				{
					init.constructionProgress += delta;
					if (init.constructionProgress >= init.constructionDuration)
					{
						Notify.message(init.title + " completed.");
						init.constructionComplete = true;
						
						Resources.currency += init.initialRewards.x*(auditSeparation/60);
						Resources.enforcement += init.initialRewards.y*(auditSeparation/60);
						Resources.legitimacy += init.initialRewards.z*(auditSeparation/60);
						
						TraitPack traitWeight = PlayerTraitTracker.generateTraitWeight(init.traits);
						PlayerTraitTracker.playerTraitsSum = PlayerTraitTracker.traitPackCombo(PlayerTraitTracker.playerTraitsSum, init.traits, init.weight, traitWeight);
						PlayerTraitTracker.maxTraitsSum = PlayerTraitTracker.traitPackCombo(PlayerTraitTracker.maxTraitsSum, PlayerTraitTracker.maxTraits, init.weight, traitWeight);
						
						
						foreach(string secInit in init.unlocks)
						{
							foreach (Initiative nextInit in initiatives)
							{
								if (nextInit.title == secInit)
								{
									nextInit.unlocked = true;
								}
							}	
						}
					}
				}	
			}
		}
	}
}
