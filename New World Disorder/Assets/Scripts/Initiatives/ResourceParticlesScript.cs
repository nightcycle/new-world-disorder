﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceParticlesScript : MonoBehaviour
{
    public ParticleSystem approvalParticles;
    public ParticleSystem militaryParticles;
    public ParticleSystem fundsParticles;

    public int approvalParticleAmount;
    public int militaryParticleAmount;
    public int fundsParticleAmount;
    public int particleSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var aPMain = approvalParticles.main;
        var mPMain = militaryParticles.main;
        var fPMain = fundsParticles.main;

        aPMain.maxParticles = approvalParticleAmount;
        aPMain.startSpeed = particleSpeed;

        mPMain.maxParticles = militaryParticleAmount;
        mPMain.startSpeed = particleSpeed;

        fPMain.maxParticles = fundsParticleAmount;
        fPMain.startSpeed = particleSpeed;
    }
}
