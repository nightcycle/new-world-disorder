﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitiativeButtonScript : MonoBehaviour
{
    public GameObject detailUI;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setDetailsActiveOrInactive()
    {
        if (detailUI.activeSelf)
        {
            Debug.Log("Now inactive object");
            detailUI.SetActive(false);
        }
        else
        {
            Debug.Log("Now active object");
            detailUI.SetActive(true);
        }
    }
}
