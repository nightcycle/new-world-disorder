﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Governors; 

namespace Initiatives
{
	public class Initiative
	{
			public string title; //the title of the initiative
			public string description; //the flavor text associated with it
			public float constructionDuration; //how long it takes to complete an initiative (in seconds)
			public bool unlocked; //whether it can be unlocked by the player
			
			public Vector3 initialConstructionCost; // x = currency, y = enforcement, z = percent,
			public Vector3 constructRates; // x = currency, y = enforcement, z = percent, This is the change per minute while under construction		
			public Vector3 earnRates; // x = currency, y = enforcement, z = percent, This is the change per minute after the initiative is completed
			public Vector3 initialRewards; // x = currency, y = enforcement, z = percent. This is the change amount upon completion of construction
			
			public float constructionProgress = 0f; //how far along in the construction the initiative is 
			public bool constructionStarted = false;
			public bool constructionComplete = false;
			public float timeSinceConstructionCompleted = 0f;
			
			public string [] unlocks; //the string array of titles of initiatives it unlocks
			
			public TraitPack traits; //the leader traits pursuing this initiative represents
			public float weight; //how much the traits should be weighted
			public int level;
			
			public Initiative(string t, bool un, string [] u)
			{
				title = t;
				unlocked = un;
				unlocks = u;
			}
	}
}
