﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GlobeScript : MonoBehaviour
{
	public GameObject camera;
	public GameObject earth;
	
	public static bool earthLocked;
	
	float x_rotation = 170;
	float y_rotation = 30;
	float fov = 43;
    float speed = 10f;
    float zoom_speed = 25f;
    // Start is called before the first frame update

    public Vector3 ComputeLocation(float longitudinalRotationDegree, float latitudinalRotationDegree)
	{
		float longitudinalRotation = (Mathf.PI*2/360)*(longitudinalRotationDegree);
		float latitudinalRotation = (Mathf.PI*2/360)*(latitudinalRotationDegree);
		
		float radius = 15; //assign this
		Vector3 globePosition = new Vector3(earth.transform.position.x, earth.transform.position.y, earth.transform.position.z); //assign this the position of the globe//apply first angle

		//setting latitudinal point
		float A = Mathf.Sin(latitudinalRotation)*radius; //a side of lat triangle
		float B = Mathf.Cos(latitudinalRotation)*radius; //b side of lat triangle
		float Bx = Mathf.Sin(longitudinalRotation)*B; //x aspect of B position
		float Bz = Mathf.Cos(longitudinalRotation)*B; //z aspect of B position

		Vector3 finalPoint = new Vector3(globePosition.x+Bx, globePosition.y+A, globePosition.x+Bz); //this should be the final global position of the point.

		return finalPoint;
	}

    public void ChangeGlobeRotationSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public void ChangeZoomSpeed(float newZoomSpeed)
    {
        zoom_speed = newZoomSpeed;
    }

    void Start()
    {
        earthLocked = false;
		earth = transform.gameObject;
		camera.transform.position = ComputeLocation(x_rotation, y_rotation);
		camera.transform.LookAt(transform.position);
		Screen.SetResolution(1366, 768, true);
    }
	
    // Update is called once per frame
    void Update()
	{
		if (earthLocked == false)
		{
			float delta = Time.deltaTime;
			float mouseInputX = Input.GetAxis("Mouse X")*delta*40;
			float mouseInputY = -Input.GetAxis("Mouse Y")*delta*40;
			float zoom = -Input.GetAxis("Mouse ScrollWheel")*delta*40;
						
			// If mouse is pressed and held, gets mouses position
			if (Input.GetMouseButton(0) && (mouseInputX != 0 || mouseInputY != 0))
			{
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

				if (Physics.Raycast(ray, out hit) && hit.transform.name == "Globe")
				{
					if (Mathf.Abs(y_rotation+(mouseInputY*speed)) < 85)
					{
						y_rotation = y_rotation + (mouseInputY*speed);
					}

					if (Mathf.Abs(x_rotation+(mouseInputX*speed)) <= 180)
					{
						x_rotation = x_rotation + (mouseInputX*speed);
					}
					else
					{
						x_rotation = (-1*x_rotation)+(Mathf.Abs(x_rotation)-180);
					}
					camera.transform.position = ComputeLocation(x_rotation, y_rotation);
					camera.transform.LookAt(transform.position);
				}
			}
			
			if ( zoom != 0)
			{
				if (fov+(zoom_speed*zoom) < 50 && fov+(zoom_speed*zoom) > 15){
					fov = fov + (zoom_speed*zoom);
					Camera.main.fieldOfView = fov;
				}	
			}
		}
    }

}
