﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Scenarios;

public class ThreatPopupScript : MonoBehaviour
{
	public static GameObject currentThreatBubble;
	private static Situation currentSituation;
	
	void Start(){
		currentThreatBubble = null;
		currentSituation = null;
	}
	
	//void Update(){
	//	if (currentSituation != null){
	//		Debug.Log("For the love of God this is actually it: " + currentSituation.situationText);
	//	}else{Debug.Log("Void");}
	//}
	
	public static void setCurrentSituation(Situation newSituation){
		currentSituation = newSituation;
	}
	
	public static Situation getCurrentSituation(){
		return currentSituation;
	}
}
