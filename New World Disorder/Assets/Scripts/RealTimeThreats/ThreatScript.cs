﻿using System.Collections;
using System.Collections.Generic;
using Scenarios;
using UnityEngine;
using UnityEngine.UI;

public class ThreatScript : MonoBehaviour
{
	public GameObject earth; //the visual globe asset
	public GameObject threatPrefab; //prefab to use as template
	public GameObject camera; //the player camera
	Camera camComponent;
	
	//UI variables
	public GameObject UI;
	public GameObject UISituationText;
	public GameObject UIResponse1Text;
	public GameObject UIResponse2Text;
	public GameObject UIResponse3Text;
	public GameObject UIResponse4Text;
	
	Transform currentThreatTransform;

	public GameObject threatCanvas;
	float originalNewThreatDelay = 15f;
	float delayReductionPerThreat = 0.975f;
	float timeSinceLastThreatMade = 4f;
	int totalThreatCount = 0;
	
	//time
	public static int threatsOwed;
	float timeSinceLastStatUpdate = 0f;
	float statUpdateRate = 0.2f; //how many times a second it update the stats
	
	//cities
	public static City c1;
	public static City c2;
	public static City c3;
	public static City c4;
	public static City c5;
	public static City c6;	
	public static City c7;
	public static City c8;
	public static City c9;
	public static City c10;
	public static City c11;
	public static City c12;

	public static City[] cities = new City[]{c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12}; 
	
	//determines where to place the threat on the planet's surface
	public Vector3 ComputeLocation(float longitudinalRotationDegree, float latitudinalRotationDegree)
	{
		float longitudinalRotation = (Mathf.PI*2/360)*(longitudinalRotationDegree);
		float latitudinalRotation = (Mathf.PI*2/360)*(latitudinalRotationDegree);
		
		float radius = (earth.transform.lossyScale.x/2)+(threatPrefab.transform.lossyScale.x/2); 
		Vector3 globePosition = new Vector3(earth.transform.position.x, earth.transform.position.y, earth.transform.position.z); //assign this the position of the globe//apply first angle

		//setting latitudinal point
		float A = Mathf.Sin(latitudinalRotation)*radius; //a side of lat triangle
		float B = Mathf.Cos(latitudinalRotation)*radius; //b side of lat triangle
		float Bx = Mathf.Sin(longitudinalRotation)*B; //x aspect of B position
		float Bz = Mathf.Cos(longitudinalRotation)*B; //z aspect of B position

		Vector3 finalPoint = new Vector3(globePosition.x+Bx, globePosition.y+A, globePosition.x+Bz); //this should be the final global position of the point.

		return finalPoint;
	}

	

	GameObject getChild(GameObject Obj, string name)
	{
		//Debug.Log(Obj.name);
		foreach(Transform o in Obj.transform){
			//Debug.Log(o.gameObject.name + " : " + name);
			if (o.gameObject.name == name)
			{
				return o.gameObject;
			}
		}
		return null;
	}
	
	public Situation findSituation(Scenario scenario, int stage)
	{
		//Debug.Log("Stage: " + stage.ToString());
		if(stage == 2){
			//Debug.Log("Assign 2");
			return scenario.s2;
		}else if(stage == 3){
			//Debug.Log("Assign 3");
			return scenario.s3;
		}else if(stage == 4){
			//Debug.Log("Assign 4");
			return scenario.s4;
		}else if(stage == 5){
			//Debug.Log("Assign 5");
			return scenario.s5;
		}else{
			//Debug.Log("Assign 1");
			return scenario.s1;
		}	
	}
	
	// Start is called before the first frame update
	void Start()
	{
		threatsOwed = 0;

		//cities
		c1 = new City("New York City", new Vector2(163.77f,40.7f));
		c2 = new City("Mexico City", new Vector2(188.48f, 19.43f));
		c3 = new City("São Paulo", new Vector2(157.06f, -46.63f));
		c4 = new City("Istanbul", new Vector2(61.50f,41.01f));
		c5 = new City("Moscow", new Vector2(53.02f, 55.75f));
		c6 = new City("Paris", new Vector2(87.69f, 48.85f));	
		c7 = new City("Delhi", new Vector2(14.19f,28.7f));
		c8 = new City("Sydney", new Vector2(-61.2f, -33.86f));
		c9 = new City("Seoul", new Vector2(-36.97f,37.56f));
		c10 = new City("Dubai", new Vector2(34.73f, 25.2f));
		c11 = new City("Cairo", new Vector2(58.77f, 30.04f));
		c12 = new City("Johannesburg", new Vector2(61.96f, -26.2f));

		cities = new City[]{c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12}; 
		
		camComponent = camera.GetComponent<Camera>();
	}

	Scenario GetScenario(string key)
	{
		if (ThreatStorage.scenarios != null){
			foreach(Scenario s in ThreatStorage.scenarios)
			{
				if (s != null && s.key == key)
				{
					return s;
				}
			}
			int i = Random.Range(0, ThreatStorage.scenarios.Length-1); //getting the array length for some reason broke it so just update this number to be the amount of scenarios - 1
			Scenario finalScenario = ThreatStorage.scenarios[i];
		
		return finalScenario;
		}
		return null;
	}

	// Update is called once per frame
	void Update()
	{
		
		//creates the visual threat
		void CreateThreat(City city, Scenario scenario) //string[] threatData
		{
			Vector3 position = ComputeLocation(city.coordinates.x, city.coordinates.y);
			GameObject threat = (GameObject)Instantiate(threatPrefab, position, Quaternion.identity);
			threat.name = city.cityName;
			RealTimeDataScript eventData = threat.GetComponent<RealTimeDataScript>();
			eventData.key = scenario.key;
			eventData.stage = 1;
			
			threat.transform.SetParent(transform);
		}   
		
		//adds new threats
		timeSinceLastThreatMade = timeSinceLastThreatMade + Time.deltaTime;
		timeSinceLastStatUpdate = timeSinceLastStatUpdate + Time.deltaTime;
		if(threatsOwed > 0 && GameOver.ended == false){
			if (timeSinceLastThreatMade > (originalNewThreatDelay*Mathf.Pow(delayReductionPerThreat, (float)totalThreatCount)))
			{
				timeSinceLastThreatMade = 0f;
				float latitude = Random.Range(0f, 359f)-90f;
				float longitude = Random.Range(-65f, 65f);
				int cityNumber = (int)Random.Range(0f, cities.Length);
				
				if (cities[cityNumber].scenario == null){
					Scenario scenario = GetScenario("");
					
					CreateThreat(cities[cityNumber], scenario);
					cities[cityNumber].scenario = scenario;
				}
			}
		}
		if (timeSinceLastThreatMade > (originalNewThreatDelay*Mathf.Pow(delayReductionPerThreat, (float)totalThreatCount)))
		{
			if (Random.Range(0f,1f) < 0.1)
			{
				threatsOwed++;				
			}
		}
		//iterates through all the threats and orients them to camera
		bool statsUpdated = false;
		foreach (Transform threatTransform in transform) 
		{
			if (timeSinceLastStatUpdate > 1/statUpdateRate)
			{
				statsUpdated = true;
				string key = threatTransform.gameObject.GetComponent<RealTimeDataScript>().key;
				int stage = threatTransform.gameObject.GetComponent<RealTimeDataScript>().stage;
				
				Scenario scenario = GetScenario(key);
				Situation situation = findSituation(scenario, stage);
					
				float currency = situation.currencyCostRate;
				float enforcement = situation.enforcementCostRate;
				float legitimacy = situation.legitimacyCostRate;
				
				Resources.currency += currency;
				Resources.enforcement += enforcement;
				Resources.legitimacy += legitimacy;
			}
			
			if (currentThreatTransform != null)
			{
				Vector3 screenPos = camComponent.WorldToScreenPoint(currentThreatTransform.position);
				screenPos = screenPos + new Vector3(0,-80,0);

                //Debug.Log(screenPos);

                //Next segment of code keeps threat ui within screen 
                if (screenPos.y > 450)
                {
                    UI.GetComponent<RectTransform>().position = new Vector3(screenPos.x,450,screenPos.z);
                }
                else if(screenPos.y < 265)
                {
                    UI.GetComponent<RectTransform>().position = new Vector3(screenPos.x, 265, screenPos.z);
                }
                else
                {
                    UI.GetComponent<RectTransform>().position = screenPos;
                }

            }
			
			RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			bool hovering = false;
            if (Physics.Raycast(ray, out hit) && hit.transform == threatTransform)
            {
				hovering = true;
				if (Input.GetMouseButtonDown(0)){
					currentThreatTransform = threatTransform;
					//UI.SetActive(true);
					UI.GetComponent<Image>().enabled = true;
					foreach(Transform child in UI.transform){
						child.gameObject.SetActive(true);
					}
					
					//fill in text to match data
					RealTimeDataScript eventData = threatTransform.gameObject.GetComponent<RealTimeDataScript>();
					//Debug.Log("Key : " + eventData.key);
					Scenario scenario = GetScenario(eventData.key);
					
					//determine scenario based on stage
					Situation situation = findSituation(scenario, eventData.stage);

					//Debug.Log("Sitch: " + situation.situationText);
					ThreatPopupScript.currentThreatBubble = currentThreatTransform.gameObject;
					ThreatPopupScript.setCurrentSituation(situation);
					//Debug.Log("CurSit: " + ThreatPopupScript.getCurrentSituation().situationText);
					
					getChild(UI, "CityName").GetComponent<Text>().text = currentThreatTransform.gameObject.name;
				
					//assign text
					UISituationText.GetComponent<Text>().text = situation.situationText;
					getChild(UI, "CurrencyText").GetComponent<Text>().text = situation.currencyCostRate + "/s";
					getChild(UI, "MilitaryText").GetComponent<Text>().text = situation.enforcementCostRate + "/s";
					getChild(UI, "ApprovalText").GetComponent<Text>().text = situation.legitimacyCostRate + "%/s";
					
					UIResponse1Text.GetComponent<Text>().text = situation.r1.responseText;
					UIResponse1Text.transform.parent.gameObject.GetComponent<ThreatButton>().responseText = situation.r1.responseText;
					
					GameObject UR1C = getChild(UIResponse1Text.transform.parent.gameObject, "CurrencyLabel");
					getChild(UR1C, "Text").GetComponent<Text>().text = situation.r1.money.ToString();
					GameObject UR1M = getChild(UIResponse1Text.transform.parent.gameObject, "MilitaryLabel");
					getChild(UR1M, "Text").GetComponent<Text>().text = situation.r1.enforcement.ToString();
					GameObject UR1A = getChild(UIResponse1Text.transform.parent.gameObject, "ApprovalLabel");
					getChild(UR1A, "Text").GetComponent<Text>().text = situation.r1.approval.ToString() +"%";
					
					UIResponse2Text.GetComponent<Text>().text = situation.r2.responseText;
					UIResponse2Text.transform.parent.gameObject.GetComponent<ThreatButton>().responseText = situation.r2.responseText;
					
					GameObject UR2C = getChild(UIResponse2Text.transform.parent.gameObject, "CurrencyLabel");
					getChild(UR2C, "Text").GetComponent<Text>().text = situation.r2.money.ToString();
					GameObject UR2M = getChild(UIResponse2Text.transform.parent.gameObject, "MilitaryLabel");
					getChild(UR2M, "Text").GetComponent<Text>().text = situation.r2.enforcement.ToString();
					GameObject UR2A = getChild(UIResponse2Text.transform.parent.gameObject, "ApprovalLabel");
					getChild(UR2A, "Text").GetComponent<Text>().text = situation.r2.approval.ToString() +"%";			
				
					UIResponse3Text.GetComponent<Text>().text = situation.r3.responseText;
					UIResponse3Text.transform.parent.gameObject.GetComponent<ThreatButton>().responseText = situation.r3.responseText;
					
					GameObject UR3C = getChild(UIResponse3Text.transform.parent.gameObject, "CurrencyLabel");
					getChild(UR3C, "Text").GetComponent<Text>().text = situation.r3.money.ToString();
					GameObject UR3M = getChild(UIResponse3Text.transform.parent.gameObject, "MilitaryLabel");
					getChild(UR3M, "Text").GetComponent<Text>().text = situation.r3.enforcement.ToString();
					GameObject UR3A = getChild(UIResponse3Text.transform.parent.gameObject, "ApprovalLabel");
					getChild(UR3A, "Text").GetComponent<Text>().text = situation.r3.approval.ToString() +"%";							
					
					UIResponse4Text.GetComponent<Text>().text = situation.r4.responseText;
					UIResponse4Text.transform.parent.gameObject.GetComponent<ThreatButton>().responseText = situation.r4.responseText;
					
					GameObject UR4C = getChild(UIResponse4Text.transform.parent.gameObject, "CurrencyLabel");
					getChild(UR4C, "Text").GetComponent<Text>().text = situation.r4.money.ToString();
					GameObject UR4M = getChild(UIResponse4Text.transform.parent.gameObject, "MilitaryLabel");
					getChild(UR4M, "Text").GetComponent<Text>().text = situation.r4.enforcement.ToString();
					GameObject UR4A = getChild(UIResponse4Text.transform.parent.gameObject, "ApprovalLabel");
					getChild(UR4A, "Text").GetComponent<Text>().text = situation.r4.approval.ToString() +"%";									
				}
			}
			
			foreach (Transform child in threatTransform)
			{
				child.rotation = camera.transform.rotation;
				child.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
				if (hovering == true){
					child.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);	
				}
			}
			
		}
		if (statsUpdated == true)
		{
			timeSinceLastStatUpdate = 0f;
		}
	}
}
