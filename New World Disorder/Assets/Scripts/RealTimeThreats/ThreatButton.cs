﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Scenarios;
using Governors; 

public class ThreatButton : MonoBehaviour
{
	public GameObject currentThreat;
	public string responseText;
	
	void Start () {
		Button btn = GetComponent<Button>();
		void TaskOnClick(){
			Situation situation = ThreatPopupScript.getCurrentSituation();
			Response[] responses = new Response[]{situation.r1, situation.r2, situation.r3, situation.r4}; 
			string text = responseText;
			
			foreach (Response response in responses) 
			{
				
				if (response.responseText == text){
					
					//change resources
					Resources.currency += response.money;
					Resources.enforcement += response.enforcement;
					Resources.legitimacy += response.approval;

					PlayerTraitTracker.playerTraitsSum = PlayerTraitTracker.traitPackCombo(PlayerTraitTracker.playerTraitsSum, response.traits, response.weight, PlayerTraitTracker.generateTraitWeight(response.traits));
					PlayerTraitTracker.maxTraitsSum = PlayerTraitTracker.traitPackCombo(PlayerTraitTracker.maxTraitsSum, PlayerTraitTracker.maxTraits, response.weight, PlayerTraitTracker.generateTraitWeight(response.traits));

					int owedThreats = (int)(10f*((1f-response.chanceOfSuccess)/2f));
					
					if (Random.Range(0f,1f) <= 0.05){
						owedThreats = owedThreats * 2;
					}
					//Debug.Log("Max Threats: " + owedThreats);
					if (Random.Range(0f,1f) <= 0.5){
						owedThreats = 0;
					}
					//Debug.Log("New Threats: " + owedThreats);
					
					ThreatScript.threatsOwed += owedThreats;
					
				
					transform.parent.GetComponent<Image>().enabled = false;
					foreach(Transform child in transform.parent){
						child.gameObject.SetActive(false);
					}
					
					Destroy(ThreatPopupScript.currentThreatBubble);
				}
			}
			

		}
	
		btn.onClick.AddListener(TaskOnClick);
	}

	
}
