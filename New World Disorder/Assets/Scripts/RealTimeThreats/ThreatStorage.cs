﻿using System.Collections;
using System.Collections.Generic;
using Scenarios;
using UnityEngine;
using System.Linq;
using Governors;

public class ThreatStorage : MonoBehaviour
{
	string tsvFilePath = "Assets/TSVs/RTT.tsv";
	
	//store scenario data here
    public static Scenario s1;
    public static Scenario s2;
    public static Scenario s3;
    public static Scenario s4;
    public static Scenario s5;

    //be sure to add all scenarios to this array
    public static Scenario[] scenarios;
	
	Response createResponse(int row, float level)
	{
		string title = TSVScript.read(tsvFilePath, row, 6);
		Response r = new Response(title);
		r.money = (int)(-1f*50*(2*level/5f)*float.Parse(TSVScript.read(tsvFilePath, row, 7))); 
		r.approval = Mathf.Round(-1f*5*(2*level/5f)*float.Parse(TSVScript.read(tsvFilePath, row, 8))); 
		r.weight = float.Parse(TSVScript.read(tsvFilePath, row, 9)); 
		
		float populist = float.Parse(TSVScript.read(tsvFilePath, row, 10));
		float honest = float.Parse(TSVScript.read(tsvFilePath, row, 11));
		float divisive = float.Parse(TSVScript.read(tsvFilePath, row, 12));
		float violent = float.Parse(TSVScript.read(tsvFilePath, row, 13));
		float autocrat = float.Parse(TSVScript.read(tsvFilePath, row, 14));
		float altruist = float.Parse(TSVScript.read(tsvFilePath, row, 15));
		float corrupt = float.Parse(TSVScript.read(tsvFilePath, row, 16));
		
		r.traits = new TraitPack(violent, autocrat, altruist, corrupt, populist, divisive, honest);	
		return r;
	}
	
	Situation createSituation(int startRow)
	{
		string situationText = TSVScript.read(tsvFilePath, startRow, 2);
		float level = float.Parse(TSVScript.read(tsvFilePath, startRow, 3));
		Situation s = new Situation(situationText);
		s.currencyCostRate = -1f*10f*(2*level/5f)*float.Parse(TSVScript.read(tsvFilePath, startRow, 4));
		s.legitimacyCostRate = Mathf.Round(100f*-1f*1.5f*(1.5f*level/5f)*float.Parse(TSVScript.read(tsvFilePath, startRow, 5)))/100f;
		
		
		s.r1 = createResponse(startRow+1, level);
		s.r2 = createResponse(startRow+2, level);
		s.r3 = createResponse(startRow+3, level);
		s.r4 = createResponse(startRow+4, level);
		return s;
	}
	
	Scenario createScenario(string title)
	{
		Scenario s = new Scenario(title);
		int startRow = TSVScript.findRow(tsvFilePath, title, 1);
		s.s1 = createSituation(startRow+1);
		s.s2 = createSituation(startRow+6);
		s.s3 = createSituation(startRow+11);
		s.s4 = createSituation(startRow+16);
		s.s5 = createSituation(startRow+21);	
		return s;
	}
	
	void Start(){

		s1 = createScenario("Corruption Scandal");
		s2 = createScenario("Labor Unions");
		s3 = createScenario("Criminal Empire");
		s4 = createScenario("Dangerous Article");
		s5 = createScenario("Rebellion");	

		scenarios = new Scenario[]{s1,s2,s3,s4,s5};
	}
	
	
	
}
