﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Governors; 

namespace Scenarios
{
	public class Response
	{
		public string responseText;
		public float chanceOfSuccess; //deprecated
		public float approval;
		public int money;
		public int enforcement;

		public TraitPack traits; //the leader traits pursuing this response represents
		public float weight; //how much the traits should be weighted
			
		//define a constructor for the class
		public Response(string rT)
		{
			responseText = rT;
		}	
	}
	
	//whats loaded into the prompts
	public class Situation
	{
		//define all of the values for the class
		public string situationText;
		public Response r1;
		public Response r2;
		public Response r3;
		public Response r4;
		
		//how much this level of situation costs per second
		public float legitimacyCostRate;
		public float currencyCostRate;
		public float enforcementCostRate;
		
		//define a constructor for the class
		public Situation(string sT)
		{
			situationText = sT;
		}
	}
	
	public class Scenario
	{
		public string key;
		public Situation s1;
		public Situation s2;
		public Situation s3;
		public Situation s4;
		public Situation s5;
		
		public Scenario(string k)
		{
			key = k;
		}
	}
	
	public class City
	{
		public string cityName;
		public Vector2 coordinates;
		public Scenario scenario;
		public int scenarioLevel = 1;
		
		public City(string cN, Vector2 c)
		{
			cityName = cN;
			coordinates = c;
		}
	}
}
//all the situations that could be escalated to