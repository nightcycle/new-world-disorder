﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RealTimeDataScript : MonoBehaviour
{
	//stores data in a gameobject
	public string key = "";
	public int stage = 1;
	
	public GameObject number;

	public float levelingDuration = 30f;
	public float timeSinceLastLevelUp = 0f;
	public float timeUntilShowAgain; //when a threat is dealt with but the response fails, this is set to a specific number where it then counts down
	public bool alerted = false;

	
	void Start(){
		timeUntilShowAgain = levelingDuration;
		if(Random.Range(0f,1f) < 0.15f){
			stage = 3;
		}
		if(Random.Range(0f,1f) < 0.15f){
			levelingDuration = 8f;
		}		
	}
	
	void Update()
	{
		if (GameOver.ended == false){
			timeSinceLastLevelUp = timeSinceLastLevelUp + Time.deltaTime;
			timeUntilShowAgain = timeUntilShowAgain - Time.deltaTime;
			if (timeUntilShowAgain < 0){
				timeUntilShowAgain = 0;
				foreach (Transform t in transform)
				{
					t.gameObject.SetActive(true);
				}
			}
			if (timeSinceLastLevelUp > levelingDuration)
			{
				timeSinceLastLevelUp = 0;
				if (stage < 5)
				{
					stage = stage + 1;
					
				}
				if(stage == 5 && alerted == false){
					alerted = true;
					Notify.message("Level 5 Threat in " + transform.name);
				}
			}
			
			number.GetComponent<TextMeshPro>().text = stage.ToString();
			if (stage == 2){
				number.GetComponent<TextMeshPro>().color = new Color(207f/255f,255f/255f,0f,1f);
				//number.GetComponent<SpriteRenderer>().sprite = num2;
			}else if (stage == 3){
				number.GetComponent<TextMeshPro>().color = new Color(255f/255f,171f/255f,0f,1f);
				//number.GetComponent<SpriteRenderer>().sprite = num3;
            }
            else if (stage == 4){
				number.GetComponent<TextMeshPro>().color = new Color(255f/255f,153f/255f,0f,1f);
				//number.GetComponent<SpriteRenderer>().sprite = num4;
			}else if (stage == 5){
				number.GetComponent<TextMeshPro>().color = new Color(255f/255f,0f,0f,1f);
				//number.GetComponent<SpriteRenderer>().sprite = num5;
            }
		}
	}
}
