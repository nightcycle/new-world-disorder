﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThreatUIExitButtonScript : MonoBehaviour
{
	public GameObject UI;
	
	void Start () {
		Button btn = GetComponent<Button>();
		btn.onClick.AddListener(TaskOnClick);
	}

	void TaskOnClick(){
		transform.parent.GetComponent<Image>().enabled = false;
		foreach(Transform child in transform.parent){
			child.gameObject.SetActive(false);
		}
	}
}
