﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
 
public class GameOver : MonoBehaviour
{
	public static bool ended;
	
	// Start is called before the first frame update
	public GameObject panel;
	public GameObject text1;
	public GameObject text2;
	public GameObject button;
			
	public GameObject [] objs;
	
	public bool enabled = false;
	
	void restartGame(){
		//Debug.Log("Click");
		if (enabled == true){
			enabled = false;
			foreach(GameObject obj in objs){
				obj.SetActive(false);
			}
		}
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
	
	void Start()
	{
		ended = false;
		objs = new GameObject [] {panel, text1, text2, button};
		button.GetComponent<Button>().onClick.AddListener(restartGame);
	}

	// Update is called once per frame
	void Update()
	{
		if (Resources.legitimacy <= 0){
			foreach(GameObject obj in objs){
				obj.SetActive(true);
				ended = true;
				GlobeScript.earthLocked = true;
			}
		}
	}
}
