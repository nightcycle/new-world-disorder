﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TSVScript : MonoBehaviour
{
	public static string read(string tsvFilePath, int row, int column){
		//subtract one because in editors the sheets start at 1.
		column--;
		row--;
		string fileData = System.IO.File.ReadAllText(tsvFilePath);
		string[] lines = fileData.Split ("\n" [0]);
		//Debug.Log(row + " : " + lines.Length);
		string[] parts = lines [row].Split ("\t" [0]);
		return parts[column];
	}	
	
	public static int findRow(string tsvFilePath, string text, int column){
		column--;
		string fileData = System.IO.File.ReadAllText(tsvFilePath);
		string[] lines = fileData.Split ("\n" [0]);
		//Debug.Log("Lines: " + lines.Length);
		for (var i = 0; i < lines.Length; i ++) {
			//Debug.Log("I: " + i);
			string[] parts = lines [i].Split ("\t" [0]);
			string currentText = parts[column];
			//Debug.Log(currentText + " : " + text);
			if (currentText.Trim() == text.Trim()){
				return i+1;//adds one because we typically subtract one above in the read method
			}
		}
		return -1;
	}
}