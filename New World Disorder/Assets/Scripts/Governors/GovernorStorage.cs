﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Governors; 

public class GovernorStorage : MonoBehaviour
{
	//get card data in a usable format
	static string tsvFilePath = "Assets/TSVs/GCT.tsv"; //location of card data
	static string fileData;
	static string [] rows;
	static string [] rowBox;
		
	//card tree
	public Card [] cardSeeds;
	
	public static Governor [] governors;


	
	// Start is called before the first frame update
	void Start()
	{
		fileData = System.IO.File.ReadAllText(tsvFilePath);
		rows = fileData.Split("\n" [0]);
		rowBox = rows[0].Split("\t" [0]);
		////Debug.Log("Creating cards");
		createCards();
		
		////////Debug.Log("Seed Count: " + cardSeeds.Length);
		governors = new Governor[4];
		
		governors[0] = generateGovernor("Soviet Europe", new string[]{"Assassination"});
		governors[1] = generateGovernor("Oceanasia", new string[]{"Illuminati"});
		governors[2] = generateGovernor("The Americas", new string[]{"Corruption"});
		governors[3] = generateGovernor("Arabian Africa", new string[]{"K-Pop STAN", "Celebrity"});	

	}	
	
	//because in this stupid language you can't change array sizes after defining them
	public Card [] updateArrayLengthCreator(Card [] formerArray)
	{
		int arrayLength = 0;
		foreach(Card formerCard in formerArray){
			if (formerCard != null){
				arrayLength += 1;
			}
		}
		
		Card [] newArray = new Card[arrayLength];
		int cardInt = 0;
		foreach(Card formerCard in formerArray){
			if(formerCard != null)
			{
				newArray[cardInt] = formerCard;
				cardInt += 1;
			}
		}
		return newArray;
	}
	
	public static TraitPack generateTraitScore(Card [] cards)
	{
		float ferocitySum = 0.5f;
		float fullFerocity = 1f;
		
		float authoritarianismSum = 0.5f;
		float fullAuthoritarianism = 1f;
		
		float humanitarianismSum = 0.5f;
		float fullHumanitarianism = 1f;
		
		float corruptionSum = 0.5f;
		float fullCorruption = 1f;
		
		float populismSum = 0.5f;
		float fullPopulism = 1f;
		
		float divisivenessSum = 0.5f;
		float fullDivisiveness = 1f;
		
		float honestySum = 0.5f;
		float fullHonesty = 1f;
		
		
		//adds the values of the card to the score
		void addCard(Card ca){
			
			if (ca.traits.ferocity != 0.5f){
				ferocitySum += (ca.weight * ca.traits.ferocity);
				fullFerocity += (ca.weight);
			}
			
			if (ca.traits.authoritarianism != 0.5f){
				authoritarianismSum += (ca.weight * ca.traits.authoritarianism);
				fullAuthoritarianism += (ca.weight);
			}
			if (ca.traits.humanitarianism != 0.5f){
				humanitarianismSum += (ca.weight * ca.traits.humanitarianism);
				fullHumanitarianism += (ca.weight);
			}
			//////Debug.Log(honesty
			
			if (ca.traits.corruption != 0.5f){
				corruptionSum += (ca.weight * ca.traits.corruption);
				fullCorruption += (ca.weight);
			}
			if (ca.traits.populism != 0.5f){
				populismSum += (ca.weight * ca.traits.populism);
				fullPopulism += (ca.weight);
			}
			if (ca.traits.divisiveness != 0.5f){
				divisivenessSum += (ca.weight * ca.traits.divisiveness);
				fullDivisiveness += (ca.weight);
			}
			if (ca.traits.honesty != 0.5f){
				honestySum += (ca.weight * ca.traits.honesty);
				fullHonesty += (ca.weight);
			}
		}
		
		//this method uses recursion to go down all the tree branches
		void census(Card c){
			if (c.children != null && c.children.Length > 0)
			{
				foreach (Card subC in c.children)
				{
					if (subC != null)
					{
						if (subC.purchased == true)
						{
							addCard(subC);
							if (subC.children.Length > 0 && subC != null){
								census(subC);
							}
						}
					}
				}
				////////Debug.Log("escaped method");
			}
		}

		//goes through seeds and maps out branches
		foreach (Card ca in cards)
		{
			if (ca != null){
				if (ca.purchased == true)
				{
					addCard(ca);
				}
				census(ca);
			}	
		}

		TraitPack actualTraits = new TraitPack(ferocitySum/fullFerocity, authoritarianismSum/fullAuthoritarianism, humanitarianismSum/fullHumanitarianism, corruptionSum/fullCorruption, populismSum/fullPopulism, divisivenessSum/fullDivisiveness, honestySum/fullHonesty);	
		
		return actualTraits;
	}
	
	public Governor generateGovernor(string region, string [] treeNames){
		////Debug.Log("Well here we go");
		string [] firstNames = {"Adrian", "Morgan", "Sidney", "Robin", "Reilly", "Jules", "Jackie", "Avery", "Dylan"};
		int firstNameCount = 9;
		string [] lastNames = {"Smith", "Johnson", "Williams", "Jones", "Brown", "Davis", "Miller", "Wilson", "Moore", "Taylor", "Anderson", "Thomas", "Jackson"};
		int lastNameCount = 13;
		
		string name = firstNames[Random.Range(0, firstNameCount)] + " " + lastNames[Random.Range(0, lastNameCount)]; //their name
		Color regionColor;
		//hard coding the names for the demo
		if (region == "The Americas"){
			name = "Felipe Carvallo";
			regionColor = new Color(74f/255f, 101f/255f, 145f/255f);
			//regionColor = Color.blue;
		}else if (region == "Oceanasia"){
			name = "Arata Misaki";
			regionColor = new Color(126f/255f, 214f/255f, 194f/255f);
			//regionColor = Color.cyan;
		}else if (region == "Soviet Europe"){
			name = "Mikhail Koltsov";
			regionColor = new Color(244f/255f, 109f/255f, 120f/255f);
			//regionColor = Color.red;
		}else{
			name = "Alyan Barakat";
			regionColor = new Color(243f/255f, 157f/255f, 110f/255f);
			//regionColor = Color.green;
		}
		
		Card [] cardsMax = new Card[1000]; //the array with a ton of empty slots
		int cardCount = 0;
		
		if (treeNames == null){
			//make a card tree
			int cardSeedCount = Random.Range(3, 8); //how many card seeds are we planting

			for(int i = 0; i < cardSeedCount; i++)
			{
				Card c = cardSeeds[Random.Range(0,cardSeeds.Length-1)];
				bool safe = true;
				foreach(Card testCard in cardsMax){
					if (c != null && testCard != null && c.description == testCard.description)
					{
						safe = false;
					}
				}
				if (c != null && safe == true){
					Card newCard = createCard(c.description);
					////////Debug.Log("Add 1: " + c.title);
					cardsMax[i] = newCard;	
					cardCount++;
				}
							
			}
		}else{
			////Debug.Log(name + " card picking");
			for(int i = 0; i < treeNames.Length; i++){
				foreach(Card seed in cardSeeds){
					////Debug.Log(seed.tree.Trim() + " : " + treeNames[i].Trim());
					if(seed.tree.Trim() == treeNames[i].Trim()){
						////Debug.Log("Found card" + seed.tree.Trim());
						cardsMax[i] = seed;
						cardCount++;
					}
				}
			}
		}
		
		void addKids(Card seed)
		{
			if (seed != null){
				//////Debug.Log("We in");
				foreach (Card c in seed.children)
				{
					Card newCard = createCard(c.description);
					cardsMax[cardCount] = newCard;
					cardCount++;
					addKids(newCard);
				}
			}
		}
		
		foreach (Card c in cardsMax)
		{
			addKids(c);
		}
		
		Card [] cards = cloneArray(updateArrayLengthCreator(cardsMax));
		
		//scrubs for duplicates
		Card [] cleanCards = new Card[cards.Length];	
		
		int runningIndex = 0;
		foreach(Card dirtCard in cards){
			bool clean = true;
			foreach(Card cleanC in cleanCards){
				if(dirtCard != null && cleanC != null && dirtCard.description == cleanC.description){
					clean = false;
				}
			}
			if(clean == true){
				cleanCards[runningIndex] = dirtCard;
				runningIndex++;
			}
		}
		
		cleanCards = cloneArray(updateArrayLengthCreator(cleanCards));
		
		//Debug.Log("Clean: " + cleanCards.Length);
		return new Governor(name, region, regionColor, generateTraitScore(cleanCards), 0, 0, 0, cleanCards);
	}

	Card [] cloneArray(Card [] template){
		Card [] newArray = new Card[template.Length];
		for(int i = 0; i < template.Length; i++){
			////////Debug.Log("Create");
			newArray[i] = createCard(template[i].description);
		}
		return newArray;
	}
	
	Card createCard(string desc){
		if (desc != ""){
			int row = TSVScript.findRow(tsvFilePath, desc, 1);
			Card c = new Card(desc);
			
			c.title = TSVScript.read(tsvFilePath, row, 2);
			c.level = (int)float.Parse(TSVScript.read(tsvFilePath, row, 4));
			c.tree = TSVScript.read(tsvFilePath, row, 3);	
			c.purchaseCost = new Vector3(Mathf.Round(50*Mathf.Pow(1.8f, c.level)*float.Parse(TSVScript.read(tsvFilePath, row, 6))), 0, Mathf.Round(10f*4f*Mathf.Pow(1.5f, c.level)*float.Parse(TSVScript.read(tsvFilePath, row, 7)))/10f);
			c.weight = float.Parse(TSVScript.read(tsvFilePath, row, 8));
			
			float populist = float.Parse(TSVScript.read(tsvFilePath, row, 9));
			float honest = float.Parse(TSVScript.read(tsvFilePath, row, 10));
			float divisive = float.Parse(TSVScript.read(tsvFilePath, row, 11));
			float violent = float.Parse(TSVScript.read(tsvFilePath, row, 12));
			float autocrat = float.Parse(TSVScript.read(tsvFilePath, row, 13));
			float altruist = float.Parse(TSVScript.read(tsvFilePath, row, 14));
			float corrupt = float.Parse(TSVScript.read(tsvFilePath, row, 15));
	
			c.traits = new TraitPack(violent, autocrat, altruist, corrupt, populist, divisive, honest);	
			
			//filling out children array		
			Card [] allChildren = new Card[1000];	
			int count = 0;
			int startColumn = 16;
						
			for(int i = 0; i < rowBox.Length+1-startColumn; i++){
				string newTitle = TSVScript.read(tsvFilePath, row, i+startColumn);
				if (newTitle.Trim() != ""){
					int newRow = TSVScript.findRow(tsvFilePath, newTitle, 2);
					string newDesc = TSVScript.read(tsvFilePath, newRow, 1);
					allChildren[count] = createCard(newDesc);
					count++;
				}
			}
			c.children = updateArrayLengthCreator(allChildren);

			if (c.children == null){
				c.children = new Card []{};
			}
			return c;
		}else{return null;}
	}
	


	void createCards(){
		//Debug.Log("Get it");
		//defining a long array to fit all of em in.
		Card [] allCards = new Card[1000];		
		int cardCount = 0;
		for(int i = 1; i <= rows.Length; i++) //starting at one to skip the first header row
		{
			string description = TSVScript.read(tsvFilePath, i, 1);

			if (description != "" && description != "Description"){
				cardCount++;
				////Debug.Log("Woah");
				allCards[i-1] = createCard(description);
			}
		}
		
		Card [] cards = updateArrayLengthCreator(allCards);
		//Debug.Log("All cards: " + cards.Length);
		//took the cards and reduced them to seeds
		Card [] allCardSeeds = new Card[1000];
		
		for(int i = 0; i < cards.Length; i++){
			Card c = cards[i];
			if (c.level == 1){
				allCardSeeds[i] = c;
			}
		}
		
		cardSeeds = updateArrayLengthCreator(allCardSeeds);

		////Debug.Log("All Seeds: " + cardSeeds.Length);
	}

}
