﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Governors; 

public class PlayerTraitTracker : MonoBehaviour
{
	public static TraitPack playerTraitsSum;
	public static TraitPack maxTraitsSum;
	public static TraitPack maxTraits;
	
	public GameObject mainTraitUI;
	public GameObject comparisonTraitUI;
	
	public static TraitPack generateTraitWeight(TraitPack t){
		float ferocity = t.ferocity;
		float authoritarianism = t.authoritarianism;
		float humanitarianism = t.humanitarianism;
		float corruption = t.corruption;
		float populism = t.populism;
		float divisiveness = t.divisiveness;
		float honesty = t.honesty;
		
		if (ferocity == 0.5f){ferocity = 0f;}else{ferocity = 1f;}
		if (authoritarianism == 0.5f){authoritarianism = 0f;}else{authoritarianism = 1f;}
		if (humanitarianism == 0.5f){humanitarianism = 0f;}else{humanitarianism = 1f;}
		if (corruption == 0.5f){corruption = 0f;}else{corruption = 1f;}
		if (populism == 0.5f){populism = 0f;}else{populism = 1f;}
		if (divisiveness == 0.5f){divisiveness = 0f;}else{divisiveness = 1f;}
		if (honesty == 0.5f){honesty = 0f;}else{honesty = 1f;}
		
		return new TraitPack(ferocity, authoritarianism, humanitarianism, corruption, populism, divisiveness, honesty);		
	}
	
	public static TraitPack traitPackCombo(TraitPack t1, TraitPack t2, float weight, TraitPack extraWeight)
	{
		if (extraWeight == null){
			extraWeight = new TraitPack(1f,1f,1f,1f,1f,1f,1f);
		}
		
		float ferocity = t2.ferocity*weight*extraWeight.ferocity;
		float authoritarianism = t2.authoritarianism*weight*extraWeight.authoritarianism;
		float humanitarianism = t2.humanitarianism*weight*extraWeight.humanitarianism;
		float corruption = t2.corruption*weight*extraWeight.corruption;
		float populism = t2.populism*weight*extraWeight.populism;
		float divisiveness = t2.divisiveness*weight*extraWeight.divisiveness;
		float honesty = t2.honesty*weight*extraWeight.honesty;
				
		return new TraitPack(t1.ferocity + ferocity, t1.authoritarianism + authoritarianism, t1.humanitarianism + humanitarianism, t1.corruption + corruption, t1.populism + populism, t1.divisiveness + divisiveness, t1.honesty + honesty);
	}

	void Start(){
		playerTraitsSum = new TraitPack(0.5f,0.5f,0.5f,0.5f,0.5f,0.5f,0.5f);
		maxTraitsSum = new TraitPack(1f,1f,1f,1f,1f,1f,1f);
		maxTraits = new TraitPack(1f,1f,1f,1f,1f,1f,1f); //referenced all over as the max score
	}

    // Update is called once per frame
    void Update()
    {
        TraitPack playerTraits = new TraitPack(playerTraitsSum.ferocity/maxTraitsSum.ferocity, playerTraitsSum.authoritarianism/maxTraitsSum.authoritarianism, playerTraitsSum.humanitarianism/maxTraitsSum.humanitarianism, playerTraitsSum.corruption/maxTraitsSum.corruption, playerTraitsSum.populism/maxTraitsSum.populism, playerTraitsSum.divisiveness/maxTraitsSum.divisiveness, playerTraitsSum.honesty/maxTraitsSum.honesty);	
		mainTraitUI.GetComponent<VisualizationUI>().traits = playerTraits;
		comparisonTraitUI.GetComponent<VisualizationUI>().traits = playerTraits;
    }
}
