﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Governors; 

public class GovernorUI : MonoBehaviour
{
	public GameObject settingsButton;
	public GameObject openButton;
	public GameObject initiativeButton;
	public GameObject mainGovernorMenu;
	public GameObject govPanelTemplate;
	public GameObject govProfilePage;
	public GameObject contentFrame;
	public GameObject backgroundButton;
	public GameObject returnButton;
	public GameObject traitVisualizationPanel;
	public GameObject favorButton;
	public GameObject cardZone;
	public GameObject cardTemplate;
	public GameObject cardContentFrame;
	
	public Governor currentGovernor;
	public string currentGovRegion;
	//public string currentCardTitle;
	//public string currentCardTree;
	
	float debounce = 0f;
	
	// Start is called before the first frame update
	void Start()
	{
		void giveFavor()
		{
			foreach(Governor g in GovernorStorage.governors)
			{
				if(g.region == currentGovRegion)
				{
					g.favor += 1;
					currentGovernor = g;
				}
			}
			
			getChild(govProfilePage, "FavorText").GetComponent<Text>().text = "Favor: " + currentGovernor.favor;
		}

		openButton.GetComponent<Button>().onClick.AddListener(openMenu);
		backgroundButton.GetComponent<Button>().onClick.AddListener(closeUI);
		returnButton.GetComponent<Button>().onClick.AddListener(openMenu);
		favorButton.GetComponent<Button>().onClick.AddListener(giveFavor);
	}

	void Update()
	{
		debounce -= Time.deltaTime;
	}

	//gets child from parent by name
	GameObject getChild(GameObject Obj, string name)
	{
		foreach(Transform o in Obj.transform){
			if (o.gameObject.name == name)
			{
				return o.gameObject;
			}
		}
		return null;
	}

	void purchaseButton(GameObject cardObj, string cardName, string cardTree, int cardLevel, Vector3 purchaseCost)
	{
		Resources.currency -= purchaseCost.x;
		Resources.enforcement -= purchaseCost.y;
		Resources.legitimacy -= purchaseCost.z;
		
		getChild(cardObj, "Panel").SetActive(false);

		foreach(Card c in getGov(currentGovRegion).cards)
		{
			if(cardName == c.title && cardTree == c.tree && cardLevel == c.level)
			{
				////Debug.Log("Buying " + c.title);
				c.purchased = true;
			}
		}
		
		TraitPack updatedTraits = GovernorStorage.generateTraitScore(getGov(currentGovRegion).cards);
		getGov(currentGovRegion).traits = updatedTraits;

		updateCards();
	}

	void createCard(Color background, bool purchased, string cardName, string cardTree, int cardLevel)
	{
		////Debug.Log("Making card: " + cardName);
		bool safe = true;
		foreach (Transform card in cardContentFrame.transform){
			if (card.name == (cardName+cardTree)){
				////Debug.Log(card.name);
				//safe = false;
				//yeah that didn't work
			}
		}
		if(safe == true){	
			foreach (Card card in getGov(currentGovRegion).cards)
			{
				if (card.title == cardName && card.tree == cardTree && card.level == cardLevel){
					GameObject newCard = Instantiate(cardTemplate, cardContentFrame.transform).gameObject;
					newCard.transform.name = (card.description);
					GameObject coverPanel = getChild(newCard, "Panel");
					Vector3 purchaseCost = card.purchaseCost;
					
					if (purchased == true)
					{
						////Debug.Log(card.title + " Purchased");
						coverPanel.SetActive(false);
					}
					else
					{
						////Debug.Log(card.title + " Not Purchased");
						getChild(coverPanel, "MoneyText").GetComponent<Text>().text = card.purchaseCost.x.ToString();
						getChild(coverPanel, "MilitaryText").GetComponent<Text>().text = card.purchaseCost.y.ToString();
						getChild(coverPanel, "ApprovalText").GetComponent<Text>().text = card.purchaseCost.z.ToString();
						getChild(coverPanel, "Button").GetComponent<Button>().onClick.AddListener(delegate{purchaseButton(newCard, cardName, cardTree, cardLevel, purchaseCost);});
					}
					getChild(newCard, "Title").GetComponent<Text>().text = card.title;
					getChild(newCard, "Description").GetComponent<Text>().text = card.description;		
				}
			}
		}		
	}
	
	//recursive method that creates the cards
	void updateCards()
	{
		Governor gov = getGov(currentGovRegion);
		foreach (Transform oldCard in cardContentFrame.transform)
		{
			Destroy(oldCard.gameObject);
		}	
	
		
		/*
		void createCards(Card card, Color background)
		{
			
			
			createCard(background, card.purchased, card.title, card.tree, card.level);
			if (card.purchased == true)
			{
				////Debug.Log(gov.name + " has purchased at some point: " + card.title);
				////Debug.Log("Length: " + card.children.Length);
				////Debug.Log(gov.name + " has this many cards: " + getGov(currentGovRegion).cards.Length);
				foreach(Card c in card.children)
				{
					////Debug.Log("Child Title: " + c.title);
					foreach (Card officialCard in getGov(currentGovRegion).cards)
					{
						////Debug.Log(officialCard.description.Trim() + " : " + c.description.Trim());
						if (officialCard.description.Trim() == c.description.Trim()){
							//Debug.Log("Level 2: " + c.title + ", " + card.children.Length);
							createCards(officialCard, background);
						}
					}
				}
			}else{
				////Debug.Log(gov.name + " doesn't have: " + card.title);
			}
		}
		*/
		////Debug.Log("Card Length: " + gov.cards.Length);
		foreach(Card card in gov.cards)
		{
			//Debug.Log("Entry: " + card.title);
			Color background = new Color(Random.Range(0f, 200f)/255f,Random.Range(0f, 200f)/255f,Random.Range(0f, 200f)/255f);
			if (card.purchased == false && card.level == 1){
				////Debug.Log(card.title + " root unlock");
				createCard(background, card.purchased, card.title, card.tree, card.level);
			}
			if (card.purchased == true){
				////Debug.Log(card.title + " purchased unlock");
				createCard(background, card.purchased, card.title, card.tree, card.level);
				bool added = false;
				foreach(Card c in card.children){
					if (c.purchased == false){
						added = true;
						createCard(background, c.purchased, c.title, c.tree, c.level);
					}
				}
			}
		}

		foreach (Transform newCard in cardContentFrame.transform)
		{
			GameObject chosenCard = null;
			foreach (Transform newCard2 in cardContentFrame.transform){
				if(getChild(newCard.gameObject, "Panel").activeSelf != getChild(newCard2.gameObject, "Panel").activeSelf){
					if(getChild(newCard.gameObject, "Panel").activeSelf == true){
						chosenCard = newCard.gameObject;
					}else{
						chosenCard = newCard2.gameObject;
					}
				}
			}
			if(chosenCard != null){
				foreach(Card cardData in gov.cards){
					if(cardData.description == chosenCard.name && cardData.purchased == true){
						Destroy(chosenCard.gameObject);
					}
				}
			}
		}

		Transform [] cardTransforms = new Transform[cardContentFrame.transform.childCount];

		float [] arr = new float [cardContentFrame.transform.childCount];
		

		float getScore(Card ca){
			float v = 0f;
			if(ca.purchased == true){
				v = v + 100000f;
			}
			v = v - Mathf.Abs((100*(float)ca.level));
			return v;
		}
		
		int sortCount = 0;
		foreach (Transform newCard in cardContentFrame.transform)
		{
			foreach(Card cardData in gov.cards){
				if(cardData.description == newCard.name){
					arr[sortCount] = getScore(cardData);
					sortCount++;
				}
			}
		}
		
		float temp; 
		
		for (int i = 0; i < arr.Length - 1; i++){
  
			// traverse i+1 to array length 
		   for (int j = i + 1; j < arr.Length; j++){
  
				// compare array element with  
				// all next element 
				if (arr[i] < arr[j]){ 
					temp = arr[i]; 
					arr[i] = arr[j]; 
					arr[j] = temp; 
				}
		   }
		}
		int finalSCount = 0;
		foreach(float sorted in arr){
			foreach (Transform sCard in cardContentFrame.transform)
			{
				foreach(Card cardData in gov.cards){
					if(sorted == getScore(cardData) && cardData.description == sCard.name){
						sCard.SetSiblingIndex(finalSCount);
						finalSCount++;
					}
				}
			}
		}

		////Debug.Log("Length: " + gov.cards.Length);
		TraitPack newTraits = GovernorStorage.generateTraitScore(gov.cards);
		////Debug.Log("Updating UI for : " + gov.name + "whose ferocity is " + GovernorStorage.governors[3].traits.ferocity);
		VisualizationUI vUI = traitVisualizationPanel.GetComponent<VisualizationUI>();
		vUI.traits = newTraits;
	}

	//closes the UI
	void closeUI()
	{
		if (debounce < 0f){
			GlobeScript.earthLocked = false;
			mainGovernorMenu.SetActive(false);
			govProfilePage.SetActive(false);
			openButton.SetActive(true);
			initiativeButton.SetActive(true);
			settingsButton.SetActive(true);
		}
	}

	Governor getGov(string reg) //do not use to set values
	{
		foreach(Governor g in GovernorStorage.governors)
		{
			////Debug.Log(g.name + " of " + g.region + " vs " + reg);
			if(g.region == reg)
			{
				return g;
			}
		}	
		return null;
	}

	//Opens the governor menu
	void openMenu()
	{
		void openProfile(string reg)
		{
			
			Governor gov = getGov(reg);
			currentGovRegion = reg;
			////Debug.Log(gov.name);
			VisualizationUI vUI = traitVisualizationPanel.GetComponent<VisualizationUI>();
			vUI.traits = gov.traits;
			
			getChild(govProfilePage, "TitleCard").GetComponent<Text>().text = gov.name;
			getChild(govProfilePage, "TitleCard").GetComponent<Text>().color = gov.regionColor;
			getChild(govProfilePage, "Image").GetComponent<Image>().color = gov.regionColor;
			getChild(govProfilePage, "FavorText").GetComponent<Text>().text = "Favor: " + gov.favor;
			
			updateCards();
			
			//TraitPack newTraits = GovernorStorage.generateTraitScore(gov.cards);
			vUI.traits = gov.traits;
			
			
			foreach(Transform line in getChild(traitVisualizationPanel, "TraitLines").transform)
			{
				line.gameObject.GetComponent<Image>().color = gov.regionColor;
			}
				
			govProfilePage.SetActive(true);
			mainGovernorMenu.SetActive(false);
		}
		
		if (debounce < 0f){
			GlobeScript.earthLocked = true;
			govProfilePage.SetActive(false);
			openButton.SetActive(false);
			initiativeButton.SetActive(false);
			settingsButton.SetActive(false);
			//clear out old governor panels
			foreach (Transform gP in contentFrame.transform)
			{
				Destroy(gP.gameObject);
			}		
			debounce = 1f; 
			
			//create new governor panels
			Governor gov1 = GovernorStorage.governors[0];
			GameObject govPanel1 = Instantiate(govPanelTemplate, contentFrame.transform);
			govPanel1.name = gov1.region;
			GameObject profButton1 = getChild(govPanel1, "Button");
			GameObject title1 = getChild(govPanel1, "GovernorName");
			GameObject region1 = getChild(govPanel1, "RegionName");		
			title1.GetComponent<Text>().text = gov1.name;
			title1.GetComponent<Text>().color = gov1.regionColor;
			region1.GetComponent<Text>().text = gov1.region;	
			region1.GetComponent<Text>().color = gov1.regionColor;
			getChild(govPanel1, "Window").GetComponent<Image>().color =  gov1.regionColor;
			profButton1.GetComponent<Button>().onClick.AddListener(() => {openProfile(govPanel1.name);});
			
			Governor gov2 = GovernorStorage.governors[1];
			GameObject govPanel2 = Instantiate(govPanelTemplate, contentFrame.transform);
			govPanel2.name = gov2.region;
			GameObject profButton2 = getChild(govPanel2, "Button");
			GameObject title2 = getChild(govPanel2, "GovernorName");
			GameObject region2 = getChild(govPanel2, "RegionName");		
			title2.GetComponent<Text>().text = gov2.name;
			title2.GetComponent<Text>().color = gov2.regionColor;
			region2.GetComponent<Text>().text = gov2.region;	
			region2.GetComponent<Text>().color = gov2.regionColor;
			getChild(govPanel2, "Window").GetComponent<Image>().color =  gov2.regionColor;
			profButton2.GetComponent<Button>().onClick.AddListener(() => {openProfile(govPanel2.name);});	

			Governor gov3 = GovernorStorage.governors[2];
			GameObject govPanel3 = Instantiate(govPanelTemplate, contentFrame.transform);
			govPanel3.name = gov3.region;
			GameObject profButton3 = getChild(govPanel3, "Button");
			GameObject title3 = getChild(govPanel3, "GovernorName");
			GameObject region3 = getChild(govPanel3, "RegionName");		
			title3.GetComponent<Text>().text = gov3.name;
			title3.GetComponent<Text>().color = gov3.regionColor;
			region3.GetComponent<Text>().text = gov3.region;	
			region3.GetComponent<Text>().color = gov3.regionColor;
			getChild(govPanel3, "Window").GetComponent<Image>().color =  gov3.regionColor;
			profButton3.GetComponent<Button>().onClick.AddListener(() => {openProfile(govPanel3.name);});
			
			Governor gov4 = GovernorStorage.governors[3];
			GameObject govPanel4 = Instantiate(govPanelTemplate, contentFrame.transform);
			govPanel4.name = gov4.region;
			GameObject profButton4 = getChild(govPanel4, "Button");
			GameObject title4 = getChild(govPanel4, "GovernorName");
			GameObject region4 = getChild(govPanel4, "RegionName");		
			title4.GetComponent<Text>().text = gov4.name;
			title4.GetComponent<Text>().color = gov4.regionColor;
			region4.GetComponent<Text>().text = gov4.region;	
			region4.GetComponent<Text>().color = gov4.regionColor;
			getChild(govPanel4, "Window").GetComponent<Image>().color =  gov4.regionColor;
			profButton4.GetComponent<Button>().onClick.AddListener(() => {openProfile(govPanel4.name);});	
			
			mainGovernorMenu.SetActive(true);
		}
	}
}
