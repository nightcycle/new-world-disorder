﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Governors; 

public class VisualizationUI : MonoBehaviour
{
	public TraitPack traits;

	public GameObject originNode;
	public GameObject adjustmentNodes;
	public GameObject lines;

	GameObject getChild(GameObject Obj, string name)
	{
		foreach(Transform o in Obj.transform){
			if (o.gameObject.name == name)
			{
				return o.gameObject;
			}
		}
		return null;
	}
	
	//moves the node in relation to origin
	void updateNode(float degree, float percentage, GameObject node){
		//percentage = 1f;
		
		//make it a bit more exciting
		float difference = Mathf.Abs(percentage - 0.5f);
		float distanceToClosestExtreme = 0.5f-difference;
		float start = 1f;
		float multiplier = 1f;
		if (percentage < 0.5){
			start = 0f;
			multiplier = -1f;
		}
		float power = Mathf.Pow(0.8f, difference*10f);
		float newPercent = 0.5f + (multiplier*(0.5f-(distanceToClosestExtreme*power)));
		percentage = newPercent;

		degree += 39.5f; //dunno why but this offset is needed
		float maxLength = 75; //max length of 
		
		float radians = degree * Mathf.Deg2Rad;
		float x = Mathf.Cos(radians)*maxLength*percentage;
		float y = Mathf.Sin(radians)*maxLength*percentage;
		
		node.transform.localPosition = originNode.transform.localPosition + new Vector3(x,y,0);
	}

	//updates UI to match traits
    public void updateUI()
	{
		//updates the lines around the nodes
		void updateLines(GameObject lineGrouping)
		{
			//aligns one line between two nodes
			void updateLine(GameObject line, GameObject node1, GameObject node2)
			{
				line.transform.localPosition = node1.transform.localPosition;
				float x = node2.transform.localPosition.x - node1.transform.localPosition.x + 0.001f;
				float y = node2.transform.localPosition.y - node1.transform.localPosition.y + 0.001f;
				//Debug.Log("X: " + x + ", Y: " + y);
				float degree = 90 + (Mathf.Atan(y/x)*Mathf.Rad2Deg);

				float getMagnitude(Vector3 v3)
				{
					float a = Mathf.Sqrt((v3.x*v3.x)+(v3.y*v3.y));
					return Mathf.Sqrt((a*a)+(v3.z*v3.z));
				}
				
				float length = getMagnitude(node2.transform.localPosition-node1.transform.localPosition);
				line.transform.GetComponent<RectTransform>().sizeDelta = new Vector2 (3, length);
				//Debug.Log("Deg: " + degree);
				//if (x != 0f && y != 0f){
				line.transform.rotation = Quaternion.Euler(0,0,degree);
				//}
				line.transform.localPosition = node1.transform.localPosition + ((node2.transform.localPosition-node1.transform.localPosition)/2);
			}	
			
			updateLine(getChild(lineGrouping, "HumAutLine"), getChild(adjustmentNodes, "HumanitarianismNode"), getChild(adjustmentNodes, "AuthoritarianismNode"));	
			updateLine(getChild(lineGrouping, "AutFerLine"), getChild(adjustmentNodes, "AuthoritarianismNode"), getChild(adjustmentNodes, "FerocityNode"));	
			updateLine(getChild(lineGrouping, "FerDivLine"), getChild(adjustmentNodes, "FerocityNode"), getChild(adjustmentNodes, "DivisivenessNode"));	
			updateLine(getChild(lineGrouping, "DivHonLine"), getChild(adjustmentNodes, "DivisivenessNode"), getChild(adjustmentNodes, "HonestyNode"));	
			updateLine(getChild(lineGrouping, "HonPopLine"), getChild(adjustmentNodes, "HonestyNode"), getChild(adjustmentNodes, "PopularityNode"));	
			updateLine(getChild(lineGrouping, "PopEliLine"), getChild(adjustmentNodes, "PopularityNode"), getChild(adjustmentNodes, "ElitismNode"));	
			updateLine(getChild(lineGrouping, "EliHumLine"), getChild(adjustmentNodes, "ElitismNode"), getChild(adjustmentNodes, "HumanitarianismNode"));			
		}
		
		if (traits != null)
		{
			//Debug.Log("Yay");
			updateNode(0f, traits.honesty, getChild(adjustmentNodes, "HonestyNode"));	
			updateNode(360f/7f, traits.populism, getChild(adjustmentNodes, "PopularityNode"));
			updateNode(2*360f/7f, traits.corruption, getChild(adjustmentNodes, "ElitismNode"));
			updateNode(3*360f/7f, traits.humanitarianism, getChild(adjustmentNodes, "HumanitarianismNode"));
			updateNode(4*360f/7f, traits.authoritarianism, getChild(adjustmentNodes, "AuthoritarianismNode"));
			updateNode(5*360f/7f, traits.ferocity, getChild(adjustmentNodes, "FerocityNode"));		
			updateNode(6*360f/7f, traits.divisiveness, getChild(adjustmentNodes, "DivisivenessNode"));	
			updateLines(lines);
		}
	}
	
	void Update()
	{
		//Debug.Log(traits.populism);
		updateUI();
	}
	
	void Start()
	{
		//traits = new TraitPack(1f, 1f,1f,1f,1f,1f,1f);
	}
}
