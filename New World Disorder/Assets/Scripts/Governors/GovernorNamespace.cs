﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Governors
{
	public class Governor
	{
		public string name; //their name
		public string region; //the region they govern
		public Color regionColor; //the color of their region
		//public Character character; //the visual body of the governor

		public TraitPack traits; //the various variables used to measure a governor
				
		public int favor; //how many favor points have been awarded to this NPC
		public float timeSinceBeingMostFavorable; //how long it has been since the NPC had the most favor points
		
		public float timeSinceLastAttack; //how long since they last struck against another NPC
		
		public Card [] cards;
		
		
		
		public Governor(string nam, string reg, Color regCol, TraitPack tra, int fav, float tSBMF, float tSLA, Card [] car)
		{
			name = nam;
			region = reg;
			regionColor = regCol;
			traits = tra;
			favor = fav;
			timeSinceBeingMostFavorable = tSBMF;
			timeSinceLastAttack = tSLA;
			cards = car;
		}
	}
	
	public class Card
	{
		public string tree; //the name of the tree, used to group cards together as to not cause confusion with duplicate titles. 
		public string title; //card title
		public string description;
		public string owner; //owner of card
		public Texture icon;
		
		public bool purchased = false;
		public Vector3 purchaseCost;// x = currency, y = enforcement, z = percent,
		
		public Card [] children; //the cards that are unlocked when this is purchased
		
		public float weight; //a general multiplier applied to each weighted trait. Please keep between 0 and 1
		public TraitPack traits; //the traits represented by this card
		
		public int level; //how deep a card is, 1 = seed
		
		public Card(string desc)
		{
			description = desc;
		}
	}
	
	public class TraitPack
	{
		public float ferocity; //Their willingness to use violence against threats
		public float authoritarianism; //How directly they try to control civillian lives
		public float humanitarianism; //How much they value living standards of civillians
		public float corruption; //How much they use the position to better themselves
		public float populism; //How much they appeal to the average person
		public float divisiveness; //How consistent people's opinion is of them
		public float honesty; //How truthful they are with the public
		
		public TraitPack(float fer, float aut, float hum, float cor, float pop, float div, float hon)
		{
			ferocity = fer;
			authoritarianism = aut;
			humanitarianism = hum;
			corruption = cor;
			populism = pop;
			divisiveness = div;
			honesty = hon;
		}
	}
	
	public class Character
	{
		//some basic visuals stuff, doesn't need to be implemented for prototype
		public Color skinColor; 
		public Color hairColor;
		public Color eyeColor;
		public Color outfitColor;
		public string hairStyle;
		public string eyeStyle;
		public string mouthStyle;
		public string noseStyle;
		public string facialHairStyle;
		public string outfitStyle;
		
		public Character(Color ski, Color hai, Color eye, Color outC, string haiS, string eyeS, string mou, string nos, string fac, string outF)
		{
			skinColor = ski;
			hairColor = hai;
			eyeColor = eye;
			outfitColor = outC;
			hairStyle = haiS;
			eyeStyle = eyeS;
			mouthStyle = mou;
			noseStyle = nos;
			facialHairStyle = fac;
			outfitStyle = outF;
		}
	}
}
