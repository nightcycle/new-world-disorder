﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsUIScript : MonoBehaviour
{
    public GameObject globeSpeedSlider;
    public GameObject zoomSpeedSlider;
    public GameObject doneButton;
    public GameObject settingsButton;
    public GameObject governorButton;
    public GameObject initiativeButton;
    // Start is called before the first frame update
    void Start()
    {
        globeSpeedSlider.SetActive(false);
        zoomSpeedSlider.SetActive(false);
        doneButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetSliderActive()
    {
        GlobeScript.earthLocked = true;
		globeSpeedSlider.SetActive(true);
        zoomSpeedSlider.SetActive(true);
        doneButton.SetActive(true);
        settingsButton.SetActive(false);
        governorButton.SetActive(false);
        initiativeButton.SetActive(false);
    }

    public void SetSliderInactive()
    {
        GlobeScript.earthLocked = false;
		globeSpeedSlider.SetActive(false);
        zoomSpeedSlider.SetActive(false);
        doneButton.SetActive(false);
        settingsButton.SetActive(true);
        governorButton.SetActive(true);
        initiativeButton.SetActive(true);
    }
}
