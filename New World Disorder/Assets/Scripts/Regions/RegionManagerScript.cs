﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionManagerScript : MonoBehaviour
{
    public ResourceParticlesScript resourceParticles;
    public int approvalGeneration = 1;
    public int militaryGeneration = 1;
    public int fundsGeneration = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //This code references the script that has all the particle systems on it and changes the amount of particles emitted by them based on current resource generation
        resourceParticles.approvalParticleAmount = resourcesToParticles(approvalGeneration);
        resourceParticles.fundsParticleAmount = resourcesToParticles(fundsGeneration);
        resourceParticles.militaryParticleAmount = resourcesToParticles(militaryGeneration);
    }

    //This function decides how many particles are emitted depending on how many resources are being generated in a region
    public int resourcesToParticles(int resourceGeneration)
    {
        int rightBound = 50;
        int conversion = 1;

        //Returns 0 if this region is making no resources
        if (resourceGeneration <= 0)
        {
            return 0;
        }

        //Returns 1 to 10 depending on the amount of resources this region is making
        while (rightBound < resourceGeneration)
        {
            if (rightBound > 450)
            {
                break;
            }
            else
            {
                conversion++;
                rightBound += 50;
            }
        }
        return conversion;

    }
}
