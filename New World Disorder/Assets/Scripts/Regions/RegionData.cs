﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionData : MonoBehaviour
{
	public string displayName; //text used for name on user facing stuff

	//the city locations
	public int cityCount;
	public string city1Name;
	public Vector2 city1Coordinates;
	public string city2Name;
	public Vector2 city2Coordinates;	
	public string city3Name;
	public Vector2 city3Coordinates;	
	public string city4Name;
	public Vector2 city4Coordinates;
	public string city5Name;
	public Vector2 city5Coordinates;
	public string city6Name;
	public Vector2 city6Coordinates;
	public string city7Name;
	public Vector2 city7Coordinates;
	public string city8Name;
	public Vector2 city8Coordinates;
	public string city9Name;
	public Vector2 city9Coordinates;
	public string city10Name;
	public Vector2 city10Coordinates;	
	
}
