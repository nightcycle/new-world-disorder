﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Resources : MonoBehaviour
{
	public static float currency;
	public static float enforcement;
	public static float legitimacy;
	public static int favor;
	
	public GameObject currencyObj;
	public GameObject enforcementObj;
	public GameObject legitimacyObj;
		
	public float timeSinceLastPunishment = 0f;
	
	void Start(){
		currency = 1000f;
		enforcement = 1000f;
		legitimacy = 75f;
		favor = 0;
	}
	
	public float timeSinceLastBroke = 100f;
	
	bool lastRun = false; //lets it update one last time when the game ends.
	
	void Update()
	{
		if (GameOver.ended != true || lastRun == false){
			if (GameOver.ended == true){
				lastRun = true;
			}
			
			if (currency <= 0){
				timeSinceLastBroke = 0f;				
			}
			timeSinceLastBroke += Time.deltaTime;
			timeSinceLastPunishment += Time.deltaTime;
			
			if (timeSinceLastPunishment > 5f){
				if(timeSinceLastBroke < timeSinceLastPunishment){
					legitimacy -= 10;
					Notify.message("Bankruptcy: -10% Approval");
					timeSinceLastPunishment = 0f;
				}
			}

			if (legitimacy > 100){
				legitimacy = 100;
			}else if (legitimacy < 0){
				legitimacy = 0;
			}
			
			if (currency <= 0){
					currency = 0;
				}
		
			currencyObj.GetComponent<Text>().text = (Mathf.Round(currency*10)/10).ToString("n0");
			//enforcementObj.GetComponent<Text>().text = (Mathf.Round(enforcement*10)/10).ToString("n0");
			legitimacyObj.GetComponent<Text>().text = (Mathf.Round(legitimacy*10)/10).ToString() + "%";
		}
	}
}
