﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notify : MonoBehaviour
{
	public GameObject notificationText;
	public GameObject futureParent;
	
	public static int integer;
	public static int ourInteger;
	public static string [] messageQueue; 
	
	float timeTillNextMessage = 0f;
	
	// Start is called before the first frame update
    void Start()
    {
		timeTillNextMessage = 0f;
        integer = 0;
		ourInteger = 0;
		messageQueue = new string[100000]; 
    }

	public static void message(string text){
		if (messageQueue == null){
			messageQueue = new string[100000]; 
		}
		//Debug.Log("Int: " + integer);
		//Debug.Log("Text: " + text);
		messageQueue[integer] = text;
		integer++;
	}

    // Update is called once per frame
    void Update()
    {
		//Debug.Log("tTNM: " + timeTillNextMessage);
		timeTillNextMessage = timeTillNextMessage - Time.deltaTime;
		if (timeTillNextMessage < 0f){
			timeTillNextMessage = 0f;
		}
		if(messageQueue[ourInteger] != null && timeTillNextMessage <= 0f){
			GameObject newText = Instantiate(notificationText, futureParent.transform);
			newText.GetComponent<Text>().text = messageQueue[ourInteger];
			timeTillNextMessage = 3f;
			ourInteger++;
		}
    }
}
